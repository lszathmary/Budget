package gui.SummaryPanel.BalanceDialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import utils.Convert;

@SuppressWarnings("serial")
public class BalanceDialog extends JDialog
{
	private JButton btnOk;
	private JTextField tPaneExpense;
	private JTextField tPaneBalance;
	private JTextField tPaneStartDate;
	private JTextField tPaneEndDate;
	private JTextField tFieldIncome;
	
	/**
	 * Constructor
	 */
	public BalanceDialog() 
	{
		setTitle("Balance");
		initialize();
		setupListeners();
	}
	
	/**
	 * Initialize the components of the Dialog.
	 */
	private void initialize()
	{
		//
		//
		//
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(530,341);
		//
		//
		//
		JPanel panelMain = new JPanel();
		getContentPane().add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(new BorderLayout(0, 0));
		//
		//
		//
		JPanel panelCenter = new JPanel();
		panelCenter.setBorder(new EmptyBorder(0, 10, 0, 10));
		panelMain.add(panelCenter);
		panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.X_AXIS));
		//
		//
		//
		JPanel panelCenterBorder = new JPanel();
		panelCenterBorder.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelCenter.add(panelCenterBorder);
		panelCenterBorder.setLayout(null);
		//
		//
		//
		tPaneExpense = new JTextField();
		tPaneExpense.setHorizontalAlignment(SwingConstants.CENTER);
		tPaneExpense.setBounds(237, 73, 130, 26);
		tPaneExpense.setPreferredSize(new Dimension(100, 26));
		tPaneExpense.setEditable(false);
		panelCenterBorder.add(tPaneExpense);
		//
		//
		//
		tPaneBalance = new JTextField();
		tPaneBalance.setHorizontalAlignment(SwingConstants.CENTER);
		tPaneBalance.setBounds(237, 119, 130, 26);
		tPaneBalance.setPreferredSize(new Dimension(100, 26));
		tPaneBalance.setEditable(false);
		panelCenterBorder.add(tPaneBalance);
		//
		//
		//
		JLabel lblIncome = new JLabel("Income");
		lblIncome.setBounds(127, 33, 92, 14);
		panelCenterBorder.add(lblIncome);
		//
		//
		//
		JLabel lblExpense = new JLabel("Expense");
		lblExpense.setBounds(127, 81, 92, 14);
		panelCenterBorder.add(lblExpense);
		//
		//
		//
		JLabel lblBalance = new JLabel("Balance");
		lblBalance.setBounds(127, 126, 92, 14);
		panelCenterBorder.add(lblBalance);
		//
		//
		tFieldIncome = new JTextField();
		tFieldIncome.setHorizontalAlignment(SwingConstants.CENTER);
		tFieldIncome.setEditable(false);
		tFieldIncome.setBounds(237, 27, 130, 26);
		panelCenterBorder.add(tFieldIncome);
		tFieldIncome.setColumns(10);
		//
		//
		//
		JPanel panelTop = new JPanel();
		panelTop.setBorder(new EmptyBorder(10, 10, 10, 10));
		panelMain.add(panelTop, BorderLayout.NORTH);
		panelTop.setLayout(new BoxLayout(panelTop, BoxLayout.X_AXIS));
		//
		//
		//
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setHgap(15);
		flowLayout.setVgap(15);
		panelTop.add(panel);
		//
		//
		//
		JLabel lblStartDate = new JLabel("Start Date:");
		panel.add(lblStartDate);
		//
		//
		//
		tPaneStartDate = new JTextField();
		tPaneStartDate.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(tPaneStartDate);
		tPaneStartDate.setEditable(false);
		tPaneStartDate.setPreferredSize(new Dimension(130,26));
		//
		//
		//
		JLabel lblEndDate = new JLabel("End Date:");
		panel.add(lblEndDate);
		//
		//
		//
		tPaneEndDate = new JTextField();
		tPaneEndDate.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(tPaneEndDate);
		tPaneEndDate.setEditable(false);
		tPaneEndDate.setPreferredSize(new Dimension(130,26));
		//
		//
		//
		JPanel panelBottom = new JPanel();
		panelBottom.setBorder(new EmptyBorder(10, 0, 10, 0));
		panelMain.add(panelBottom, BorderLayout.SOUTH);
		//
		//
		//
		btnOk = new JButton("OK");
		btnOk.setPreferredSize(new Dimension(100,26));
		panelBottom.add(btnOk);
	}
	
	/**
	 * Register the event listeners.
	 */
	private void setupListeners()
	{
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOk_actionPerformed(e);
			}
		});
	}
	
	/**
	 * Handles the "OK" button`s ActionEvent that occurred.
	 * @param event - The ActionEvent that occurred.
	 */
	private void btnOk_actionPerformed(ActionEvent event)
	{
		this.setVisible(false);
	}
		
	/**
	 * Sets the start date that is displayed in the dialog box.
	 * @param value - The value that is displayed in the dialog.
	 */
	public void setStartDate(Date date)
	{
		if (date != null)
		{
			this.tPaneStartDate.setText(Convert.DateToStringMonth(date));
		}
		else
		{
			this.tPaneStartDate.setText("");
		}
	}
		
	/**
	 * Sets the end date that is displayed in the dialog box.
	 * @param value - The value that is displayed in the dialog.
	 */
	public void setEndDate(Date date)
	{
		if (date != null)
		{
			this.tPaneEndDate.setText(Convert.DateToStringMonth(date));
		}
		else
		{
			this.tPaneEndDate.setText("");
		}
	}
		
	/**
	 * Sets the income value that is displayed in the dialog box.
	 * @param value - The value that is displayed in the dialog.
	 */
	public void setIncomeValue(BigDecimal value)
	{
		this.tFieldIncome.setText(value.toString());
	}
	
	/**
	 * Sets the expense value that is displayed in the dialog box.
	 * @param value - The value that is displayed in the dialog.
	 */
	public void setExpenseValue(BigDecimal value)
	{
		this.tPaneExpense.setText(value.toString());
	}
	
	/**
	 * Sets the balance value that is displayed in the dialog box.
	 * @param value - The value that is displayed in the dialog.
	 */
	public void setBalanceValue(BigDecimal value)
	{
		this.tPaneBalance.setText(value.toString());
	}
	
	public void resetDialog()
	{
		this.setStartDate(null);
		this.setEndDate(null);
		
		this.setIncomeValue(new BigDecimal(0));
		this.setExpenseValue(new BigDecimal(0));
		this.setBalanceValue(new BigDecimal(0));
	}
	
	/**
	 * Displays the dialog. Sets the location of the window relative to the 
	 * specified component. 
	 * @return - returns true if the user clicked the "OK" button, otherwise return false.
	 */
	public void showDialog(Component component)
	{
	 	setLocationRelativeTo(component);
	 	setVisible(true);
	}
}
