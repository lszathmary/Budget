package gui.SummaryPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import dataManager.Category;
import gui.SummaryPanel.summaryTable.BigDecimalTableCellRenderer;
import gui.SummaryPanel.summaryTable.SummaryTableModel;
import gui.timelinePanel.timelineTable.CategoryTableCellRenderer;

/**
 * OverviewPanel class
 *	Represents the "SummaryPanel" tab in the main application window.
 */
@SuppressWarnings("serial")
public class SummaryPanel extends JPanel
{
	protected JButton btnDetails;					
	
	protected JTable summaryTable;					
	protected SummaryTableModel summaryModel;		
	
	protected JComboBox<String> cboxPeriod;			
	protected JComboBox<String> cboxTransactions;	 
	
	protected JDatePanelImpl datePanelStartDate;	
	protected JDatePickerImpl datePickerStartDate;	
	
	protected JDatePanelImpl datePanelEndDate;		
	protected JDatePickerImpl datePickerEndDate;	

	protected DefaultCategoryDataset chartDataset;
	protected CustomBarRenderer barRenderer;
		
	
	/**
	 * Constructor
	 */
	public SummaryPanel()
	{
		setSize(new Dimension(750,400));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		//
		// panel
		//
		JPanel panel = new JPanel();
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		//
		// panelTop
		//
		JPanel panelTop = new JPanel();
		panelTop.setBorder(new EmptyBorder(10, 20, 10, 20));
		panel.add(panelTop, BorderLayout.NORTH);
		//
		// datePickerStartDate
		//
		UtilDateModel model = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		datePanelStartDate = new JDatePanelImpl(model,p);
		//
		// datePickerEndDate
		//
		UtilDateModel model2 = new UtilDateModel();
		Properties p2 = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		datePanelEndDate = new JDatePanelImpl(model2,p2);
		//
		// labelPeriod
		//
		JLabel lblPeriod = new JLabel("Period: ");
		panelTop.add(lblPeriod);
		//
		// comboBoxPeriod
		//
		DefaultComboBoxModel<String> modelPeriod = new DefaultComboBoxModel<String>();
		modelPeriod.addElement("All");
		modelPeriod.addElement("Today");
		modelPeriod.addElement("Yesterday");
		modelPeriod.addElement("This Week");
		modelPeriod.addElement("Last Week");
		modelPeriod.addElement("This Month");
		modelPeriod.addElement("Last Month");
		modelPeriod.addElement("This Year");
		modelPeriod.addElement("Custom");
		cboxPeriod = new JComboBox<String>();
		cboxPeriod.setModel(modelPeriod);
		cboxPeriod.setLightWeightPopupEnabled(false);
		cboxPeriod.setPreferredSize(new Dimension(110, 26));
		panelTop.add(cboxPeriod);
		//
		// lblStartDate
		//
		JLabel lblStartDate = new JLabel("   Start Date: ");
		panelTop.add(lblStartDate);
		datePickerStartDate = new JDatePickerImpl(datePanelStartDate, new DateLabelFormatter());
		datePickerStartDate.setPreferredSize(new Dimension(160, 28));
		datePickerStartDate.getComponent(0).setPreferredSize(new Dimension(100,28));
		datePickerStartDate.getComponent(1).setPreferredSize(new Dimension(30,28));
		panelTop.add(datePickerStartDate);
		//
		// lblEndDate
		//
		JLabel lblEndDate = new JLabel("   End Date:");
		panelTop.add(lblEndDate);
		datePickerEndDate = new JDatePickerImpl(datePanelEndDate, new DateLabelFormatter());
		datePickerEndDate.setPreferredSize(new Dimension(160, 28));
		datePickerEndDate.getComponent(0).setPreferredSize(new Dimension(100,28));
		datePickerEndDate.getComponent(1).setPreferredSize(new Dimension(30,28));
		panelTop.add(datePickerEndDate);
		//
		// panelCenter
		//
		JPanel panelCenter = new JPanel();
		panelCenter.setBorder(new EmptyBorder(10, 0, 10, 20));
		panel.add(panelCenter, BorderLayout.CENTER);
		panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.X_AXIS));
		//
		// panelBottom
		//
		JPanel panelBottom = new JPanel();
		panelBottom.setBorder(new EmptyBorder(0, 100, 10, 30));
		panel.add(panelBottom, BorderLayout.SOUTH);
		panelBottom.setLayout(new BoxLayout(panelBottom, BoxLayout.X_AXIS));
		//
		// panelBottomLeft
		//
		JPanel panelBottomLeft = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelBottomLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panelBottom.add(panelBottomLeft);
		//
		// cboxTransactions
		//
		DefaultComboBoxModel<String> cbModel = new DefaultComboBoxModel<String>();
		cbModel.addElement("Expense");
		cbModel.addElement("Income");
		cboxTransactions = new JComboBox<String>(cbModel);
		panelBottomLeft.add(cboxTransactions);
		cboxTransactions.setModel(cbModel);
		
		cboxTransactions.setPreferredSize(new Dimension(110, 26));
		//
		// panelBottomRight
		//
		JPanel panelBottomRight = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panelBottomRight.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		panelBottom.add(panelBottomRight);
		//
		//
		//
		btnDetails = new JButton("Balance");
		btnDetails.setPreferredSize(new Dimension(100,26));
		panelBottomRight.add(btnDetails);
		//
		// panelLeft
		//
		JPanel panelLeft = new JPanel();
		panelLeft.setBorder(new EmptyBorder(10, 20, 10, 20));
		panel.add(panelLeft, BorderLayout.WEST);
		panelLeft.setLayout(new BoxLayout(panelLeft, BoxLayout.X_AXIS));
		//
		// scrollPane
		//
		JScrollPane scrollPane = new JScrollPane();
		panelLeft.add(scrollPane);
		scrollPane.setViewportBorder(null);
		scrollPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setPreferredSize(new Dimension(330,300));
		scrollPane.getViewport().setBackground(UIManager.getColor("Table.background"));
		//
		// table
		//
		summaryModel = new SummaryTableModel();
		summaryTable = new JTable(summaryModel);
		summaryTable.setBorder(null);
		summaryTable.setDefaultRenderer(Category.class, new CategoryTableCellRenderer());
		summaryTable.setDefaultRenderer(BigDecimal.class, new BigDecimalTableCellRenderer());
		summaryTable.setTableHeader(null);
		summaryTable.setShowGrid(false);
		summaryTable.setBorder(null);
		summaryTable.setRowSelectionAllowed(false);
		summaryTable.setRowHeight(36);
		summaryTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		summaryTable.getColumnModel().getColumn(1).setPreferredWidth(30);
		summaryTable.setBackground(UIManager.getColor("Table.background"));
		scrollPane.setViewportView(summaryTable);
		//
		// initialize BarChart
		//
		initializeBarChart(panelCenter);
	}
	
	/**
	 * Initialize the Bar Chart.
	 * @param panel - The panel to which to add the chart.
	 */
	private void initializeBarChart(JPanel panel)
	{
		chartDataset = new DefaultCategoryDataset();
		
		// create the bar chart
		final JFreeChart chart = ChartFactory.createBarChart("", "", "", chartDataset,                  
	            PlotOrientation.VERTICAL, false, true, false);
		
		// customize appearance of the chart
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		
		plot.setOutlinePaint(null);
		plot.getDomainAxis().setTickLabelFont(new Font(UIManager.getFont("Table.font").getName(), Font.PLAIN, 9));
		plot.getRangeAxis().setTickLabelFont(new Font(UIManager.getFont("Table.font").getName(), Font.PLAIN, 9));
		plot.setBackgroundPaint(UIManager.getColor("Table.background"));

		// set background color
		chart.setBackgroundPaint(UIManager.getColor("Table.background"));

		// set the bar renderer
		barRenderer = new CustomBarRenderer();
		chart.getCategoryPlot().setRenderer(barRenderer);
						
		// add chart to ChartPanel
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		// add the ChartPanel to the panel
		panel.add(chartPanel);
	}
	
	
	/**
	 * Set the the date of "Start Date" date picker component.
	 * @param date - The new date of the "Start Date" date picker component.
	 */
	protected void setStartDate(Date date)
	{
		if (date == null) 
		{
			datePanelStartDate.getModel().setValue(null);
			
			return;
		}
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
						
		datePanelStartDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), 
				calendar.get(Calendar.DAY_OF_MONTH));
		
		datePanelStartDate.getModel().setSelected(true);
	}
	
	/**
	 * Get the the date of "Start Date" date picker component.
	 * @return
	 */
	protected Date getStartDate()
	{
		return (Date) datePickerStartDate.getModel().getValue();
	}
	
	/**
	 * Set the the date of "End Date" date picker component.
	 * @param date
	 */
	protected void setEndDate(Date date)
	{
		if (date == null) 
		{
			datePanelEndDate.getModel().setValue(null);
			
			return;
		}
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
	
		datePanelEndDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), 
				calendar.get(Calendar.DAY_OF_MONTH));
		
		datePanelEndDate.getModel().setSelected(true);
	}
	
	/**
	 * Get the the date of "End Date" date picker component.
	 * @return
	 */
	protected Date getEndDate()
	{
		return (Date) datePickerEndDate.getModel().getValue();
	}

	/**
	 * Enables or disables "Start Date" and "End Date" date picker components, depending on the 
	 * value of the parameter.  
	 */
	protected void setEnabledDateChange(boolean bool)
	{
		datePickerStartDate.getComponent(1).setEnabled(bool);
		datePickerEndDate.getComponent(1).setEnabled(bool);
	}
	
	/**
	 * Handle the conversion both from an Object to a String, and back from a 
	 * String to an Object. Needed for DatePicker.
	 */
	private class DateLabelFormatter extends AbstractFormatter 
	{
	    private String datePattern = "  dd MMMM yyyy";
	    private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);
	    
	    public Object stringToValue(String text) throws ParseException 
	    {
	        return dateFormatter.parseObject(text);
	    }

	    public String valueToString(Object value) throws ParseException 
	    {
	        if (value != null) 
	        {
	            Calendar calendar = (Calendar) value;
	            return dateFormatter.format(calendar.getTime());
	        }

	        return "";
	    }
	}
}
