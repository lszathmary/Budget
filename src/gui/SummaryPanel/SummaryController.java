package gui.SummaryPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import dataManager.Category;
import dataManager.DataManager;
import dataManager.TransactionChangedEvent;
import dataManager.TransactionChangedListener;
import gui.SummaryPanel.BalanceDialog.BalanceDialog;
import utils.DateUtils;

/**
 * OverViewController class
 * The OverViewController is the coordinator for the "Overview" portion of my application. When 
 * 	the user works in the "Overview" tab of the application, the OverViewController handles the 
 * 	interactions with the user, displaying dialogs, working with other controllers, and updating 
 * 	OverViewController related data.
 */
@SuppressWarnings("serial")
public class SummaryController extends SummaryPanel 
{
	private DataManager dataManager;
	private BalanceDialog balanceDialog;
		
	/**
	 * Constructor
	 * @param dataManager - The reference to the DataManager class.
	 */
	public SummaryController(DataManager dataManager)
	{
		this.dataManager = dataManager;
		
		balanceDialog = new BalanceDialog();
		
		this.setEnabledDateChange(false);			
		
		setupListeners();
		refresh();
	}
	
	/**
	 * Register event handler functions.
	 */
	private void setupListeners()
	{
		//
		// DataManager - valueChanged
		//
		dataManager.addTransactionListener(new TransactionChangedListener() {
			public void valueChanged(TransactionChangedEvent event){
				dataManager_valueChanged(event);
			}
		});
		//
		//	cboxPeriod - actionPerformed
		//
		cboxPeriod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cboxPeriod_actionPerformed(arg0);
			}
		});
		// 
		// datePicker_StartDate - actionPerformed
		//
		datePickerStartDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datePicker_StartDate_actionPerformed(arg0);
			}
		});
		//
		// datePicker_EndDate - actionPerformed
		//
		datePickerEndDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datePicker_EndDate_actionPerformed(arg0);
			}
		});
		//
		//	comboBox - actionPerformed
		//
		cboxTransactions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cboxTransactions_actionPerformed(arg0);
			}
		});
		//
		// btnHelp - actionPerformed
		//
		btnDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnDetails_actionPeformed(arg0);
			}
		});	
	}
	
	/**
	 * Handles the DataManager`s event that occurred.
	 * A transaction has changed or has been added or has been removed.
	 * @param event - the TransactionChangedEvent that occurred.
	 */
	private void dataManager_valueChanged(TransactionChangedEvent event)
	{
		refresh();
	}
	
	/**
	 * Handles the "cboxTransactions" ActionEvent that occurred.
	 * The user changed the transaction type (income/expanse) which to display.
	 * @param e - the ActionEvent that occurred.
	 */
	private void cboxTransactions_actionPerformed(ActionEvent e)
	{
		refresh();
	}
	
	/**
	 * Handles the "cboxPeriod" ActionEvent that occurred.
	 * The user has changed the time period of the transaction to display.
	 * @param e - the ActionEvent that occurred.
	 */
	private void cboxPeriod_actionPerformed(ActionEvent e)
	{
		Date startDate = new Date();
		Date endDate = new Date();

		// display all the transaction in the "TimeLine" table
		if (cboxPeriod.getSelectedItem().toString() == "All")
		{
			this.setEnabledDateChange(false);
		}

		// display the transactions that happened today 
		if (cboxPeriod.getSelectedItem().toString() == "Today")
		{
			startDate = new Date();
			endDate = new Date();
			
			this.setEnabledDateChange(false);
		}

		// display the transactions that happened yesterday
		if (cboxPeriod.getSelectedItem().toString() == "Yesterday")
		{
			startDate = DateUtils.getYesterday();
			endDate = DateUtils.getYesterday();
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened this week
		if (cboxPeriod.getSelectedItem().toString() == "This Week")
		{
			startDate = DateUtils.getFirstDayOfWeek(new Date());
			endDate = DateUtils.getLastDayOfWeek(new Date());
			
			this.setEnabledDateChange(false);
		}

		// display the transactions that happened last week
		if (cboxPeriod.getSelectedItem().toString() == "Last Week")
		{
			startDate = DateUtils.getFirstDayOfWeek(DateUtils.getLastWeek());
			endDate = DateUtils.getLastDayOfWeek(DateUtils.getLastWeek());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened last week
		if (cboxPeriod.getSelectedItem().toString() == "Last Week")
		{
			startDate = DateUtils.getFirstDayOfWeek(DateUtils.getLastWeek());
			endDate = DateUtils.getLastDayOfWeek(DateUtils.getLastWeek());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened this month
		if (cboxPeriod.getSelectedItem().toString() == "This Month")
		{
			startDate = DateUtils.getFirstDayOfMonth(new Date());
			endDate = DateUtils.getLastDayOfMonth(new Date());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened last month
		if (cboxPeriod.getSelectedItem().toString() == "Last Month")
		{
			startDate = DateUtils.getFirstDayOfMonth(DateUtils.getLastMonth());
			endDate = DateUtils.getLastDayOfMonth(DateUtils.getLastMonth());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened this year
		if (cboxPeriod.getSelectedItem().toString() == "This Year")
		{
			startDate = DateUtils.getFirstDayOfYear(new Date());
			endDate = DateUtils.getLastDayOfYear(new Date());
			
			this.setEnabledDateChange(false);
		}
					
		// allow the user to select the period of the transactions
		if (cboxPeriod.getSelectedItem().toString() == "Custom")
		{
			startDate = this.getStartDate();
			endDate = this.getEndDate();
			
			this.setEnabledDateChange(true);
		}
		
		setStartDate(startDate);
		setEndDate(endDate);
		refresh();
	}
		
	/**
	 * Handles the "Start Date" picker`s ActionEvent that occurred.
	 * The time period currently set to 'Custom' and the user has changed the start date.
	 * @param e - the ActionEvent that occurred.
	 */
	private void datePicker_StartDate_actionPerformed(ActionEvent e)
	{
		this.loadTransactionSummary(getStartDate(), getEndDate());
	}
	
	/**
	 * Handles the "End Date" picker`s ActionEvent that occurred.
	 * The time period currently set to 'Custom' and the user has changed the end date.
	 * @param e - the ActionEvent that occurred.
	 */
	private void datePicker_EndDate_actionPerformed(ActionEvent e)
	{
		this.loadTransactionSummary(getStartDate(), getEndDate());
	}
	
	/**
	 * Handles the "Add" button ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnDetails_actionPeformed(ActionEvent e)
	{
		balanceDialog.resetDialog();
		
		// setup the 'Balance' Dialog
		if (dataManager.getTransactionCount() != 0)
		{
			balanceDialog.setStartDate(getStartDate());
			balanceDialog.setEndDate(getEndDate());

			// calculate balance
			BigDecimal income = dataManager.getSumOfIncomeTransactions(getStartDate(), getEndDate());
			BigDecimal expense = dataManager.getSumOfExpenseTransactions(getStartDate(), getEndDate());
			BigDecimal balance = income.subtract(expense);
			
			balanceDialog.setIncomeValue(income);
			balanceDialog.setExpenseValue(expense);
			balanceDialog.setBalanceValue(balance);
		}
		
		balanceDialog.showDialog(this);
	}
	
	/**
	 * Clear the data from the Table and the BarChart.	
	 */
	private void clear()
	{
		// reset the bar chart data set
		chartDataset.clear();
		
		// remove all rows for table 
		summaryModel.RemoveAllRows();
	}
	
	/**
	 * Refresh the Bar Chart and the and the 'Summary' Table. 
	 */
	private void refresh()
	{
		if (cboxPeriod.getSelectedItem().toString() == "All")
		{
			// in case there are no transaction do not display any start date and end date
			if (dataManager.getTransactionCount() == 0)
			{
				setStartDate(null);
				setEndDate(null);
			}
			else 
			{
				setStartDate(dataManager.getFirstTransaction().getDate());
				setEndDate(dataManager.getLastTransaction().getDate());
			}
		}
		
		loadTransactionSummary(getStartDate(), getEndDate());
	}
	
	/**
	 * Load summary of the transactions in the Table and the Bar chart.
	 */
	private void loadTransactionSummary(Date startDate, Date endDate)
	{
		// index of the bar in the chart
		int barIndex = 0;
		
		LinkedHashMap<Category,BigDecimal> summary = new LinkedHashMap<Category,BigDecimal>();
	
		clear();

		// get the summary of expense transactions
		if (cboxTransactions.getSelectedItem().toString() == "Expense")
		{
			summary = dataManager.getExpenseTransactionsSummary(startDate, endDate);
		}

		// get the summary of income transactions
		if (cboxTransactions.getSelectedItem().toString() == "Income")
		{
			summary = dataManager.getIncomeTransactionSummary(startDate, endDate);
		}
	
		// for each category in the summary
		for (HashMap.Entry<Category, BigDecimal> entry : summary.entrySet()) 
		{
			BigDecimal value = entry.getValue();
			Category category = entry.getKey();
			
		    // add category name and value to the chart
		    chartDataset.addValue(value, "Category", category.getName());
		    
		    // set the color of the bar in the Bar Chart
		    barRenderer.setBarColor(barIndex,category.getColor());
		    
		    

		    // add category and value to the Table
		    summaryModel.addRows(category,value);
		    		    
		    barIndex++;
		}
		
	}

}
