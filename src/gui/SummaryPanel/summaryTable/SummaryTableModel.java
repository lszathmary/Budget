package gui.SummaryPanel.summaryTable;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.table.AbstractTableModel;

import dataManager.Category;

/**
 * SummaryTableModel class
 *
 */
@SuppressWarnings("serial")
public class SummaryTableModel  extends AbstractTableModel 
{
	// List of columns in the TableModle
	private String[] columnNames = {"", ""};

	// List of rows in the table model
	private ArrayList<Category> rowList = new ArrayList<>();

	// Map of category names and the corresponding sums.
	private HashMap<String,BigDecimal> rowMap = new HashMap<String,BigDecimal>();
	
	/**
	 * Add a new row to the table model.
	 */
	public void addRows(Category category, BigDecimal value) 
	{
		rowMap.put(category.getName(), value);
		rowList.add(category);
		
		this.fireTableDataChanged();
    }
	
	/**
	 * Remove all the rows from the table model.
	 */
	public void RemoveAllRows()
	{
		this.rowMap.clear();
		this.rowList.removeAll(rowList);
		
		this.fireTableDataChanged();
	}

	@Override
	public int getColumnCount() 
	{
		return this.columnNames.length;
		
	}

	@Override
	public int getRowCount() 
	{
		return this.rowList.size();
		
	}
	
	@Override
	public String getColumnName(int columnIndex) 
	{
		return columnNames[columnIndex];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Category row = rowList.get(rowIndex);
		Object value = null;
		
		switch (columnIndex) 
        {
        	 case 0:
        		 value = row;
        		 break;
        		 
        	 case 1:
        		 value = rowMap.get(row.getName());
        		 break;
        }
		
		return value;
	}
	
	 @Override
	 public Class<?> getColumnClass(int column) 
	 {
	      switch (column) 
	      {
	          case 0:
	              return Category.class;
	              
	          case 1:
	              return BigDecimal.class;
	              
	          default:    
	        	  return Object.class;
	      }
	 }
}
