package gui.SummaryPanel.summaryTable;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * BigDecimalTableCellRenderer class
 * 	Responsible for rendering the BigDecimal classes in a JTable.
 */
@SuppressWarnings("serial")
public class BigDecimalTableCellRenderer extends DefaultTableCellRenderer
{
	public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
	{
		this.setHorizontalAlignment(RIGHT);

		setText(value.toString() + "     ");
		
		
		if (isSelected) 
        {
    		setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } 
        else 
        {
        	setBackground(table.getBackground());
            setForeground(table.getForeground());
        }
		
		
		return this;
	}
}
