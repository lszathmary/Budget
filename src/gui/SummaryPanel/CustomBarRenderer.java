package gui.SummaryPanel;

import java.awt.Color;
import java.awt.Paint;
import java.util.ArrayList;

import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;

/**
 * CustomBarRenderer
 *	Responsible for rendering bars in the BarChart.
 */

public class CustomBarRenderer extends BarRenderer 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 976000016250911217L;
	private ArrayList<Color> colors;	// the colors of the bars

    /**
     * Constructor.
     */ 
    public CustomBarRenderer() 
    {
        this.colors = new ArrayList<Color>();
        this.setBarPainter(new StandardBarPainter());
        
        setMaximumBarWidth(.04); 
    }

    /**
     * Returns the paint for an item.  Overrides the default behavior inherited from
     * AbstractSeriesRenderer.
     *
     * @param row - the series.
     * @param column - the category.
     *
     * @return The item color.
     */
    
    @Override
    public Paint getItemPaint(final int row, final int column) 
    {
   	
    	if (colors.get(column) != null)
    	{
    		
    		
    		
    		return (colors.get(column));
    	}
    	else
    	{
    		return Color.magenta;
    	}
    }
    
    /**
     * Sets the color of the bar with the specified index.
     * @param index - The index of the bar.
     * @param color - The color of the bar.
     */
    public void setBarColor(int index, Color color)
    {
    	colors.add(index, color);
    }
}
