package gui;



import java.awt.Toolkit;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import dataManager.DataManager;
import gui.SummaryPanel.SummaryController;
import gui.budgetPanel.BudgetController;
import gui.timelinePanel.TimeLineController;

/**
 * BudgetView class
 * 	Represents the application`s main window.
 */
@SuppressWarnings("serial")
public class BudgetFrame extends JFrame 
{
	private TimeLineController timeLinePanelController;
	private BudgetController budgetsPanelController;
	private SummaryController overViewController; 
	
	private DataManager dataManager;
	
	/**
	 * Constructor.
	 */
	public BudgetFrame()
	{
		setTitle("Budget");
		setIcon();
		setLookandFeel();
		
		dataManager = new DataManager();
		dataManager.initialize();

		timeLinePanelController = new TimeLineController(dataManager);
		budgetsPanelController = new BudgetController(dataManager);
		overViewController = new SummaryController(dataManager);
		
		initialize();
	}
	
	/**
	 * Set the application`s icon.
	 */
	private void setIcon()
	{
		try
		{
			setIconImage(Toolkit.getDefaultToolkit().getImage(new File(".").getCanonicalPath()  
							+ "/budget.png"));
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}
	
	/**
	 * Set the look and feel of the application.
	 */
	private void setLookandFeel()
	{
		
		
 		try 
 		{
 			// Set the native system look and feel
 			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
 			//UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
 			//UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
 			//UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
 			//UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
 			
 		} 
 		catch (UnsupportedLookAndFeelException e) 
 		{
 			// handle exception
 		}
 		catch (ClassNotFoundException e) 
 		{
 			// handle exception
 		}
 		catch (InstantiationException e) 
 		{
 			// handle exception
 		}
 		catch (IllegalAccessException e) 
 		{
 			// handle exception
 		}

	}
	
	/**
	 * Initialize the components of the BudgetFrame.
	 */
	private void initialize()
	{
		this.setBounds(100, 100, 976, 571);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));
		//
		//	tabbedPane
		//
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		this.getContentPane().add(tabbedPane);
		//
		//	panelTimeLine
		//
		tabbedPane.addTab("Timeline", this.timeLinePanelController );
		//
		// panelBudgets
		//
		tabbedPane.addTab("Budgets", this.budgetsPanelController);
		//
		// panelOverview
		//
		tabbedPane.addTab("Summary", this.overViewController);
	}
	
}
