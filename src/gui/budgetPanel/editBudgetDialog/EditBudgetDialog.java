package gui.budgetPanel.editBudgetDialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import dataManager.Category;
import dataManager.Transaction;
import gui.budgetPanel.addBudgetDialog.CategoryTableModel;
import gui.timelinePanel.timelineTable.BigDecimalTableCellRenderer;
import gui.timelinePanel.timelineTable.CategoryTableCellRenderer;
import gui.timelinePanel.timelineTable.DateTableCellRenderer;
import gui.timelinePanel.timelineTable.StringTableCellRenderer;
import gui.timelinePanel.timelineTable.TimeLineTableModel;
import gui.timelinePanel.timelineTable.TimelineTableRowSorter;

/**
 * EditBudgetDialog class.
 *	The dialog used for editing a new budget. It also used to display the transaction that met the
 * criterias of the budget.
 */
@SuppressWarnings("serial")
public class EditBudgetDialog extends JDialog 
{
	private JTable tableTimeLine;					//
	private JTable tableCategory;					//
													//  Table
	private CategoryTableModel categoryModel;		//
	private TimeLineTableModel timeLineModel;		//
	
	private JButton btnSave;						// Button
	private JButton btnDelete;						//
	
	private JTextField textFieldName;				//
	private JFormattedTextField textFieldValue; 	// TextField
	private JTextArea textAreaNote; 				//
	
	protected JDatePanelImpl datePanelStartDate;	// 
	protected JDatePickerImpl datePickerStartDate;	// DatePickers
	protected JDatePanelImpl datePanelEndDate;		//	
	protected JDatePickerImpl datePickerEndDate;	//
	
	private int dialogResult;	//  
	
	
	public static final int SAVE = 1;
	public static final int DELETE = -1;
	
	/**
	 * Create a new instance of the edit dialog.
	 */
	public EditBudgetDialog() 
	{
		Initialize();
		setupListeners();
		setupTimeLineDataTable();
	}
	
	/**
	 *  Initialize the components of the Dialog
	 */
	public void Initialize()
	{
		// 
		// Dialog
		//
		timeLineModel = new TimeLineTableModel();
		setResizable(false);
		setSize(new Dimension(645, 480));
		setModalityType(ModalityType.APPLICATION_MODAL);
	    //
		// tabbedPane
		//
	    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	    tabbedPane.setBorder(null);
	    tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	    getContentPane().add(tabbedPane, BorderLayout.CENTER);
		categoryModel = new  CategoryTableModel();
		
		 //
	    // panelTimeLine
	    //
	    JPanel panelTimeLine = new JPanel();
	    tabbedPane.addTab("Timeline", null, panelTimeLine, null);
	    panelTimeLine.setBorder(new EmptyBorder(0, 0, 10, 0));
	    panelTimeLine.setLayout(new BorderLayout(0, 0));
	    // 
		// panelTimeLineMain
		//
	    JPanel panelTimeLineMain = new JPanel();
	    panelTimeLineMain.setBorder(new EmptyBorder(20, 20, 10, 20));
	    panelTimeLine.add(panelTimeLineMain, BorderLayout.CENTER);
	    panelTimeLineMain.setLayout(new BoxLayout(panelTimeLineMain, BoxLayout.X_AXIS));
	    //
	    // scrollPane2
	    //
	    JScrollPane scrollPane2 = new JScrollPane();
	    panelTimeLineMain.add(scrollPane2);
	    //
	    // tableTimeLine
	    //
	    tableTimeLine = new JTable();
	    scrollPane2.setViewportView(tableTimeLine);
	    tableTimeLine.setModel(timeLineModel);
	    tableTimeLine.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    tableTimeLine.setDefaultRenderer(Category.class, new CategoryTableCellRenderer());
	    tableTimeLine.setDefaultRenderer(BigDecimal.class, new BigDecimalTableCellRenderer());
	    tableTimeLine.setDefaultRenderer(Date.class, new DateTableCellRenderer());
	    tableTimeLine.setDefaultRenderer(String.class, new StringTableCellRenderer());
	    tableTimeLine.setAutoCreateRowSorter(true);
	    tableTimeLine.setRowSorter(new  TimelineTableRowSorter(timeLineModel));
	    tableTimeLine.setRowHeight(36);
	    //
	    //
		//
		DecimalFormat decimalformat = new DecimalFormat();
		decimalformat.setMinimumFractionDigits(2);
		decimalformat.setMaximumFractionDigits(2);
		NumberFormatter numberformmatter = new NumberFormatter(decimalformat);
		numberformmatter.setValueClass(BigDecimal.class);
		DefaultFormatterFactory factory = new DefaultFormatterFactory(numberformmatter);
		//
		// datePickerStartDate
		//
		UtilDateModel model1 = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		datePanelStartDate = new JDatePanelImpl(model1,p);
		//
		// datePickerEndDate
		//
		UtilDateModel model2 = new UtilDateModel();
		Properties p2 = new Properties();
		p2.put("text.today", "Today");
		p2.put("text.month", "Month");
		p2.put("text.year", "Year");
		datePanelEndDate = new JDatePanelImpl(model2,p2);
	    //
	    // panelBudget
	    //
	    JPanel panelBudget = new JPanel();
	    tabbedPane.addTab(" Edit    ", null, panelBudget, null);
	    panelBudget.setBorder(new EmptyBorder(10, 20, 0, 20));
	    panelBudget.setLayout(new BorderLayout(0, 0));
	    //
	    //  panelBudgetMain
	    //
	    JPanel panelBudgetMain = new JPanel();
	    panelBudgetMain.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
	    panelBudget.add(panelBudgetMain, BorderLayout.CENTER);
	    panelBudgetMain.setLayout(null);
	    //
	    // scrollPaneBudget
	    //
	    JScrollPane scrollPane1 = new JScrollPane();
	    scrollPane1.setBounds(214, 42, 362, 304);
	    panelBudgetMain.add(scrollPane1);
	    //
	    // lblCategory
	    //
	    JLabel lblCategory = new JLabel("Category:");
	    lblCategory.setBounds(214, 16, 179, 14);
	    panelBudgetMain.add(lblCategory);
	    //
	    //  tableCategory
	    //
	    tableCategory = new JTable();
	    tableCategory.setModel(categoryModel);
	    tableCategory.setDefaultRenderer(Category.class, new CategoryTableCellRenderer());
	    tableCategory.setTableHeader(null);
	    tableCategory.setRowHeight(36);
	    this.tableCategory.getColumnModel().getColumn(0).setPreferredWidth(250);
	    this.tableCategory.getColumnModel().getColumn(1).setPreferredWidth(80);
	    scrollPane1.setViewportView(tableCategory);
	    //
	    // lblName
	    //
	    JLabel lblName = new JLabel("Name:");
	    lblName.setBounds(16, 17, 98, 14);
	    panelBudgetMain.add(lblName);
	    //
	    // textFieldName
	    //
	    textFieldName = new JTextField();
	    textFieldName.setText("");
	    textFieldName.setColumns(10);
	    textFieldName.setBounds(16, 42, 170, 26);
	    panelBudgetMain.add(textFieldName);
	    //
	    // lblValue
	    //
	    JLabel lblValue = new JLabel("Value:");
	    lblValue.setBounds(16, 79, 98, 14);
	    panelBudgetMain.add(lblValue);
	    //
	    // textFieldValue
	    //
	    textFieldValue = new JFormattedTextField();
	    textFieldValue.setFormatterFactory(factory);
	    textFieldValue.setHorizontalAlignment(SwingConstants.RIGHT);
	    textFieldValue.setBounds(16, 104, 170, 26);
	    panelBudgetMain.add(textFieldValue);
	    //
	    // lblNote
	    //
	    JLabel lblNote = new JLabel("Note:");
	    lblNote.setBounds(16, 261, 98, 14);
	    panelBudgetMain.add(lblNote);
	    //
	    // textAreaNote
	    //
	    textAreaNote = new JTextArea();
	    textAreaNote.setBorder(BorderFactory.createLineBorder(Color.lightGray));
	    textAreaNote.setText("");
	    textAreaNote.setLineWrap(true);
	    //textAreaNote.setBorder(UIManager.getBorder("TextField.border"));
	    textAreaNote.setBounds(16, 286, 169, 60);
	    panelBudgetMain.add(textAreaNote);
	    datePickerStartDate = new JDatePickerImpl(datePanelStartDate, new DateLabelFormatter());
	    datePickerStartDate.setBounds(16, 166, 170, 24);
	    datePickerStartDate.getComponent(0).setPreferredSize(new Dimension(160,24));
	    datePickerStartDate.getComponent(1).setPreferredSize(new Dimension(40,24));
	    panelBudgetMain.add(datePickerStartDate);
	    //
	    // lblStartDate
	    //
	    JLabel lblStartDate = new JLabel("Start Date:");
	    lblStartDate.setBounds(16, 141, 98, 14);
	    panelBudgetMain.add(lblStartDate);
	    //
	    // lblEndDate
	    //
	    JLabel lblEndDate = new JLabel("End Date:");
	    lblEndDate.setBounds(16, 201, 98, 14);
	    panelBudgetMain.add(lblEndDate);
	    datePickerEndDate = new JDatePickerImpl(datePanelEndDate, new DateLabelFormatter());
	    datePickerEndDate.setBounds(16, 226, 170, 24);
	    datePickerEndDate.getComponent(0).setPreferredSize(new Dimension(160,24));
	    datePickerEndDate.getComponent(1).setPreferredSize(new Dimension(40,24));
	    panelBudgetMain.add(datePickerEndDate);
	    //
	    // panelBottom
	    //
	    JPanel panelBudgetBottom = new JPanel();
	    panelBudgetBottom.setBorder(new EmptyBorder(5, 0, 5, 0));
	    panelBudget.add(panelBudgetBottom, BorderLayout.SOUTH);
	    panelBudgetBottom.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5));
	    //
	    // btnSave
	    //
	    btnSave = new JButton("Save");
	    btnSave.setPreferredSize(new Dimension(100, 26));
	    panelBudgetBottom.add(btnSave);
	    //
	    // btnDelete
	    //
	    btnDelete = new JButton("Delete");
	    btnDelete.setPreferredSize(new Dimension(100, 26));
	    panelBudgetBottom.add(btnDelete);
	   
	}

	/**
	 * Setup the "TimeLine" table;
	 */
	private void setupTimeLineDataTable()
	{
		this.tableTimeLine.getColumnModel().getColumn(0).setPreferredWidth(50);
		this.tableTimeLine.getColumnModel().getColumn(0).setMaxWidth(80);
				
		this.tableTimeLine.getColumnModel().getColumn(1).setPreferredWidth(230);
		this.tableTimeLine.getColumnModel().getColumn(1).setMaxWidth(330);
		
		this.tableTimeLine.getColumnModel().getColumn(2).setPreferredWidth(120);
		this.tableTimeLine.getColumnModel().getColumn(2).setMaxWidth(150);
		
		this.tableTimeLine.getColumnModel().getColumn(3).setPreferredWidth(180);
		
		this.tableTimeLine.getColumnModel().getColumn(4).setPreferredWidth(140);
		this.tableTimeLine.getColumnModel().getColumn(4).setMaxWidth(200);
		
		this.tableTimeLine.removeColumn(tableTimeLine.getColumnModel().getColumn(5));
	}
		
	/**
	 * Register the event listeners.
	 */
	private void setupListeners()
	{
		//
		// btnSave
		//
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnSave_actionPerformed(event);
			}
		});
		//
		// btnDelete
		//
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnDelete_actionPerformed(event);
			}
		});
	}
	
	/**
	 * Handles the "Save" button`s ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnSave_actionPerformed(ActionEvent event)
	{
		if (this.getName().equals(""))
		{
			JOptionPane.showMessageDialog(this, " Please enter a name for the budget ! ");
			
			return;
		}
		
		
		if (this.getName().length() > 30)
		{
			JOptionPane.showMessageDialog(this, " The name of the budget must not be longer" +
					"than 30 charachters !" );
			
			return;
		}
		
		
		if (getStartDate().compareTo(getEndDate()) > 0 )
		{
			JOptionPane.showMessageDialog(this,
					"The start date must not be greater than the end date !");
			
			return;
		}
		
		if (this.getValue().compareTo(new BigDecimal(0)) <= 0 )
		{
			JOptionPane.showMessageDialog(this,
					"The budgets value must be greater than 0 !");
			
			return;
		}
		
		
		if (this.getSelectedCategories().size() == 0 )
		{
			JOptionPane.showMessageDialog(this,
					"You must select at least one category !");
			
			return;
		}
		
		
		this.dialogResult = 1;
		this.setVisible(false);
	}
	
	/**
	 * Handles the "Delete" button`s ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnDelete_actionPerformed(ActionEvent event)
	{
		int result = JOptionPane.showConfirmDialog(this,
							"Are you sure that you want to delete this budget ?",
							"Delete", JOptionPane.OK_CANCEL_OPTION);
		
		if (result == JOptionPane.OK_OPTION)
		{
			this.dialogResult = -1;
			this.setVisible(false);
		}
	}
	
	/**
	 * Set the name that will be displayed in the dialog box.
	 * @param name - the name to display in the dialog box.
	 */
	public void setName(String name)
	{
		this.textFieldName.setText(name);
	}
	
	/**
	 * Gets the name that is displayed in the dialog.
	 * @return - the name that is displayed in the dialog. 
	 */
	public String getName()
	{
		return this.textFieldName.getText().trim();
	}
	
	/**
	 * Sets the value that is displayed in the dialog.
	 * @param value - amount that is displayed in the dialog.
	 */
	public void setValue(BigDecimal value)
	{
		this.textFieldValue.setValue(value);
	}
	
	/**
	 * Gets the value that is displayed in the dialog.
	 * @return - the value that is displayed in the dialog.
	 */
	public BigDecimal getValue()
	{
		BigDecimal value = (BigDecimal)textFieldValue.getValue();
		
		// set scale to 2 so the number look like this #.##
		return value.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}
	
	/**
	 * Sets the note that is displayed in the dialog
	 * @param note - the note to display.
	 */
	public void setNote(String note)
	{
		this.textAreaNote.setText(note);
	}
	
	/**
	 * Gets the note that is display in the dialog 
	 * @return the note that is displayed.
	 */
	public String getNote()
	{
		return this.textAreaNote.getText();
	}
	
	
	/**
	 * Sets the start date that will be displayed in the dialog.
	 * @param date - the start date to display in the dialog. 
	 */
	public void setStartDate(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		datePanelStartDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		datePanelStartDate.getModel().setSelected(true);
	}
		
	/**
	 * Gets the start date that is displayed in the dialog.
	 * @return - the date that is displayed in the dialog. 
	 */
	public Date getStartDate()
	{
		return (Date) datePickerStartDate.getModel().getValue();
	}
	
	/**
	 * Sets the date that will be displayed in the dialog.
	 * @param date
	 */
	public void setEndDate(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		datePanelEndDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		datePanelEndDate.getModel().setSelected(true);
	}
	
	/**
	 * Gets the date that is displayed in the "Transaction" dialog. 
	 */
	public Date getEndDate()
	{
		return (Date) datePickerEndDate.getModel().getValue();
	}
	
	/**
	 * Set the selected categories, the other categories will be deselected. 
	 * @param categories - selected categories.
	 */
	public void setSelectedCategories(List<Category> categories)
	{
		categoryModel.deselectAll();
		
		for (int i=0 ; i< categories.size(); i++)
		{
			this.categoryModel.setSelected(categories.get(i), true);
		}
	}
	
	/**
	 * Gets the list of the categories that are selected.
	 * @return - selected categories.
	 */
	public ArrayList<Category> getSelectedCategories()
	{
		return this.categoryModel.getSelected();
	}

	/**
	 * Set the list of the categories that the user can select from.
	 * @param categories - list of available categories.
	 */
	public void setCategories(List<Category> categories)
	{
		this.categoryModel.RemoveAllRows();
		
		for (int i=0 ; i< categories.size(); i++)
		{
			this.categoryModel.addRows(categories.get(i));
		}
	}
	
	/**
	 * Add a transaction to the TimeLine table.
	 */
	public void AddTransaction(Transaction transaction)
	{
		timeLineModel.addRow(transaction);
	}
	
	/**
	 * Add a transaction to the TimeLine table.
	 */
	public void setTransactions(ArrayList<Transaction> transactions)
	{
		for (Transaction transaction:transactions)
		{
			timeLineModel.addRow(transaction);
		}
	}
	
	/**
	 * Reset the values displayed in the dialog to default.
	 */
	public void resetValues()
	{
		timeLineModel.removeAllRows();
		
		setName("");
		setValue(new BigDecimal(0));
		setNote("");
		setStartDate(new Date());
		setEndDate(new Date());
			
		dialogResult = 0;
	}
	
	
	/**
	 * Displays the dialog. Sets the location of the window relative to the 
	 * specified component. 
	 * @return - returns true if the user clicked the "OK" button, otherwise return false.
	 */
	public int showDialog(Component component)
	{
	 	setLocationRelativeTo(component);
	 	setVisible(true);
	 	
	 	return dialogResult;
	}
	
	/**
	 * Handle the conversion both from an Object to a String, and back from a 
	 * String to an Object. Needed for DatePicker.
	 */
	private class DateLabelFormatter extends AbstractFormatter 
	{
	    private String datePattern = " dd MMMM yyyy";
	    private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);
	    
	    /**
	     *  Converts date from String.
	     */
	    public Object stringToValue(String text) throws ParseException 
	    {
	        return dateFormatter.parseObject(text);
	    }

	    /**
	     * Converts date to String. 
	     */
	    public String valueToString(Object value) throws ParseException 
	    {
	        if (value != null) 
	        {
	            Calendar calendar = (Calendar) value;
	            return dateFormatter.format(calendar.getTime());
	        }

	        return "";
	    }
	}
}
