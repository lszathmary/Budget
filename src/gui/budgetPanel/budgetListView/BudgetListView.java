package gui.budgetPanel.budgetListView;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import dataManager.Budget;

/**
 * BudgetView class
 *	A custom component used to display information about a list of the budgets. More specifically
 * it contains a list of BudgetListItem.
 */
@SuppressWarnings("serial")
public class BudgetListView extends JPanel 
{
	private ArrayList<Budget> budgetList; 	// list of the Budgets		

	private HashMap<Integer,BudgetListItem> budgetListItemMap; 	// map of the BudgetListItems	

	private ArrayList<BudgetItemClickListener> clickListenersList; 	// the listeners attached to the BudgetListeItems. 
	
	/**
	 * Create a new instance of the BudgetListView component.
	 */
	public BudgetListView()
	{
		budgetList = new ArrayList<Budget>();
		budgetListItemMap = new HashMap <Integer, BudgetListItem>();
		
		clickListenersList = new ArrayList<BudgetItemClickListener>();
	}
	
	/**
	 * Add a budget to the BudgetListView.
	 * @param budget - the budget to add to the BudgetListView.
	 */
	public void addBudget(Budget budget)
	{
		BudgetListItem budgetListItem = new BudgetListItem(this, budget);
	
		// Add the BudgetListItem to the JPanel.
		this.add(budgetListItem, -1);
		
		this.revalidate();
		this.repaint();
		
		budgetList.add(budget);
		budgetListItemMap.put(budget.getID(), budgetListItem);
	}
	
	/**
	 * Remove a budget from the BudgetListView.
	 * @param budget - the budget to remove from the BudgetView.
	 */
	public void removeBudget(Budget budget)
	{
		// Remove the BudgetListItem from the JPanel.
		remove(budgetListItemMap.get(budget.getID()));
		
		this.revalidate();
		this.repaint();
		
		budgetList.remove(budget);
		budgetListItemMap.remove(budget.getID());
	}	

	/**
	 * Verifies if any of budgets has changed and if so updates the related components.  
	 */
	public void refresh()
	{
		for(HashMap.Entry<Integer, BudgetListItem> entry : budgetListItemMap.entrySet())
		{
			entry.getValue().refresh();
		}
		
		this.revalidate();
		this.repaint();
	}

	/**
	 * Add a listener to the BudgetListView. 
	 * @note
	 * 	The listener is added to every BudgetListViewItem.
	 * @param listener - the listener to add.
	 */
	public void addClickListener(BudgetItemClickListener listener)
	{
		clickListenersList.add(listener);
	}
		
	/**
	 * Remove a listener to the BudgetListView.
	 * @note
	 * 	The listener is added to every BudgetListViewItem. 
	 * @param listener - the listener to remove.
	 */
	public void removeClickListener(BudgetItemClickListener listener)
	{
		clickListenersList.remove(listener);
	}
	
	/**
	 * Notifies all listeners that have registered interest for notification on this event type. 
	 * @param event - the BudgetItemClickEvent object.
	 */
	protected void fireClickPerformed(BudgetItemClickEvent event)
	{
		for (BudgetItemClickListener listener : clickListenersList) 
		{
			listener.clickPeformed(event);
		}
	}
}
