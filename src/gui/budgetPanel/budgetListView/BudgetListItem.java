package gui.budgetPanel.budgetListView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;

import dataManager.Budget;
import utils.Convert;

/**
 * BudgetItem class
 * 	A custom component used to display information about a specific budget.
 */
@SuppressWarnings("serial")
public class BudgetListItem extends JPanel
{
	private JButton btnEdit;		// Button
		
	private JLabel lblName;			// 			
	private JLabel lblTop;			// Labels	
	private JLabel lblStartDate;	//	
	private JLabel lblEndDate;		//
	
	private JProgressBar progBarBudget;	// ProgressBar

	private Budget budget;			// The budget that is displayed in this item.				
	private BudgetListView owner;	// The BudgetListView that owns this component.					

	
	/**
	 * Creates an instance of a BudgetItem component.
	 * @param owner - the BudgetView component that owns this BudgetItem.
	 */
	public BudgetListItem(BudgetListView owner, Budget budget) 
	{
		this.Initialize();
		this.setupListeners();
		
		this.owner = owner;
		this.budget = budget;
		
		this.refresh();
	}

	/**
	 * Initialize the components of the BudgetItem.
	 */
	public void Initialize()
	{
		//
		// JComponent 
		//
		setOpaque(true);
		setBackground(UIManager.getColor("Table.background"));
		setSize(new Dimension(600, 100));
		setMinimumSize(new Dimension(100, 100));
		setPreferredSize(new Dimension(565, 100));
		setMaximumSize(new Dimension(3000,100));
		setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0};
		gridBagLayout.columnWidths = new int[]{0, 200, 0};
		setLayout(gridBagLayout);
		//
		// lblName
		//
		lblName = new JLabel("New label");
		lblName.setFont(new Font(lblName.getFont().getName(),
					Font.BOLD, lblName.getFont().getSize()));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.gridwidth = 2;
		gbc_lblName.anchor = GridBagConstraints.LINE_START;
		gbc_lblName.insets = new Insets(5, 15, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		gbc_lblName.weightx = 0.5;
		gbc_lblName.weighty = 0.1;
		add(lblName, gbc_lblName);
		//
		// lblTop
		//
		lblTop = new JLabel("New label");
		lblTop.setSize(lblName.getSize());
		GridBagConstraints gbc_lblTop = new GridBagConstraints();
		gbc_lblTop.insets = new Insets(5, 5, 5, 5);
		gbc_lblTop.gridx = 1;
		gbc_lblTop.gridy = 0;
		add(lblTop, gbc_lblTop);
		//
		// btnEdit
		//
		btnEdit = new JButton("...");
		btnEdit.setPreferredSize(new Dimension(40,24));
		btnEdit.setMaximumSize(new Dimension(40,24));
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.gridwidth = 2;
		gbc_btnEdit.anchor = GridBagConstraints.LINE_END;
		gbc_btnEdit.insets = new Insets(5, 5, 5, 15);
		gbc_btnEdit.gridx = 2;
		gbc_btnEdit.gridy = 0;
		gbc_btnEdit.weightx = 0.5;
		gbc_btnEdit.weighty = 0.1;
		add(btnEdit, gbc_btnEdit);
		//
		// progBarBudget
		//
		progBarBudget = new JProgressBar();
		progBarBudget.setStringPainted(true);
		
		progBarBudget.setValue(0);
		progBarBudget.setMinimumSize(new Dimension(100,24));
		progBarBudget.setMaximumSize(new Dimension(5000,24));
		GridBagConstraints gbc_progBarBudget = new GridBagConstraints();
		gbc_progBarBudget.fill = GridBagConstraints.HORIZONTAL;
		gbc_progBarBudget.insets = new Insets(5, 20, 5, 20);
		gbc_progBarBudget.gridx = 0;
		gbc_progBarBudget.gridy = 1;
		gbc_progBarBudget.gridwidth = 6;		// specify the number of cells the component uses
		gbc_progBarBudget.weighty = 1;
		add(progBarBudget, gbc_progBarBudget);
		//
		// lblStartDate
		//
		lblStartDate = new JLabel("New label");
		GridBagConstraints gbc_lblStartDate = new GridBagConstraints();
		gbc_lblStartDate.anchor = GridBagConstraints.LINE_START;
		gbc_lblStartDate.insets = new Insets(5, 15, 5, 5);
		gbc_lblStartDate.gridx = 0;
		gbc_lblStartDate.gridy = 2;
		gbc_lblStartDate.weightx = 0.5;
		gbc_lblStartDate.weighty = 0.1;
		add(lblStartDate, gbc_lblStartDate);
		//
		// lblEndDate
		//
		lblEndDate = new JLabel("New label");
		GridBagConstraints gbc_lblEndDate = new GridBagConstraints();
		gbc_lblEndDate.gridwidth = 2;
		gbc_lblEndDate.anchor = GridBagConstraints.LAST_LINE_END;
		gbc_lblEndDate.insets = new Insets(5, 5, 5, 15);
		gbc_lblEndDate.gridx = 2;
		gbc_lblEndDate.gridy = 2;
		gbc_lblStartDate.weightx = 0.5;
		gbc_lblStartDate.weighty = 0.1;
		add(lblEndDate, gbc_lblEndDate);
		//
		// separator
		//
	}
	

	/**
	 * Register event the listeners.
	 */
	private void setupListeners()
	{
		//
		//	btnEdit - actionPerformed
		//
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnEdit_actionPerformed(event);
			}
		});
	}
	
	
	/**
	 * Handles the "..." button`s ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnEdit_actionPerformed(ActionEvent event)
	{
		owner.fireClickPerformed(new BudgetItemClickEvent(BudgetListItem.this));
	}
	
	/**
	 * Sets the budget that will be displayed in this component.
	 * @param budget 
	 */
	public void setBudget(Budget budget)
	{
		this.budget = budget;
		
		this.refresh();
	}
	
	/**
	 * Gets the budget that is displayed in this component.
	 * @return 
	 */
	public Budget getBudget()
	{
		return this.budget;
	}
		
	/**
	 * Verifies if the budget has changed and if so updates the related component.
	 * This method will automatically be called on this component by the the BudgetListView that
	 * owns this component. 
	 */
	public void refresh()
	{
		setName(budget.getName());
		setStartDate(budget.getStartDate());
		setEndDate(budget.getEndDate());
		setValues(budget.getCurrentValue(),budget.getValue());
		
		this.revalidate();
		this.repaint();
	}
	
	/**
	 * Sets the name that is displayed by the BudgetListItem.
	 * @param name - the name to display.
	 */
	public void setName(String name)
	{
		if (!lblName.getText().equals(name))
		{
			lblName.setText(name);
		}
	}
	
	/**
	 * Set the current value of the ProgressBar displayed by BudgetListItem.
	 * @param value - the current value of the ProgressBar
	 */
	public void setValue(BigDecimal value)
	{
		if (progBarBudget.getValue() != value.intValue())
		{
			progBarBudget.setValue(value.intValue());
		}
	}
	
	/**
	 * Set the the maximum value of the ProgressBar displayed by BudgetListItem.
	 * @param maxValue - the maximum value of the ProgressBar
	 */
	public void setMaximumValue(BigDecimal maxValue)
	{
		if (progBarBudget.getMaximum() != maxValue.intValue() )
		{
			progBarBudget.setMaximum(maxValue.intValue());
		}
	}
	
	/**
	 * Sets the current value and a maximum value displayed in the BudgetListItem.
	 * The method also update the values of the ProgressBar.
	 * @param value - the current value of the budget.
	 * @param maxValue - the maximum value of the budget.
	 */
	public void setValues(BigDecimal value, BigDecimal maxValue)
	{
		setMaximumValue(maxValue);
		setValue(value);
		
		if (lblTop.getText() != "[" + value.toString() + "/" + maxValue.toString() + "]")
		{	
			lblTop.setText("[" +value.toString() + "/" + maxValue.toString() + "]");
		}
	}
		
	/**
	 * Sets the StartDate is displayed by the BudgetListItem.
	 * @param startDate - the date to display.
	 */
	public void setStartDate(Date startDate)
	{
		if (lblStartDate.getText() != Convert.DateToString(startDate))
		{
			lblStartDate.setText(Convert.DateToString(startDate));
		}
	}
	
	/**
	 * Sets the EndDate is displayed by the BudgetListItem.
	 * @param endDate - the date to display.
	 */
	public void setEndDate(Date endDate)
	{
		if (lblEndDate.getText() != Convert.DateToString(endDate))
		{
			lblEndDate.setText(Convert.DateToString(endDate));
		}
	}

}
