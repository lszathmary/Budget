package gui.budgetPanel.budgetListView;

import dataManager.Budget;

/**
 * BudgetItemClickEvent 
 *	Occurs when the button on a BudgetListViewItem is clicked.
 */
public class BudgetItemClickEvent 
{
	private BudgetListItem source;
	
	/**
	 * Creates a new instance of the BudgetItemClickEvent .
	 * @param source - The BudgetListItem that raised the event.
	 */
	public BudgetItemClickEvent(BudgetListItem source)
	{
		this.source = source;
	}

	/**
	 * Gets the BudgetListItem that raised this event.
	 * @return The source of the event.
	 */
	public BudgetListItem getSource()
	{
		return this.source;
	}
	
	/**
	 * Returns the budget related to the BudgetListItem that raised this event. 
	 */
	public Budget getBudget()
	{
		return this.source.getBudget();
	}
		
}
