package gui.budgetPanel.budgetListView;

/**
 * BudgetItemClickListener class
 * 	The class is used to process the event that is raised when the button located on a 
 * BudgetListItem is clicked. 
 */
public interface BudgetItemClickListener 
{
	/**
	 * Invoked when the BudgetItemClickEvent is raised by a BudgetListItem.
	 * @param event the BudgetItemClickEvent.
	 */
	public void clickPeformed(BudgetItemClickEvent event);
}
