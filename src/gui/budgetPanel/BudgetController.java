package gui.budgetPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import dataManager.Budget;
import dataManager.DataManager;
import dataManager.TransactionChangedEvent;
import dataManager.TransactionChangedListener;
import gui.budgetPanel.addBudgetDialog.AddBudgetDialog;
import gui.budgetPanel.budgetListView.BudgetItemClickEvent;
import gui.budgetPanel.budgetListView.BudgetItemClickListener;
import gui.budgetPanel.editBudgetDialog.EditBudgetDialog;

/**
 * BudgetController class
 * 		The BudgetController is the coordinator for the "Budgets" portion of my application. When 
 * the user works in the "Budgets" tab of the application, the BudgetController handles the 
 * interactions with the user, displaying dialogs, working with other controllers, and updating 
 * Budgets related data.
 */
@SuppressWarnings("serial")
public class BudgetController extends BudgetPanel 
{
	private DataManager dataManager;
	
	private AddBudgetDialog addBudgetDialog;
	private EditBudgetDialog editBudgetDialog;
	
	/**
	 * Create a new instance of the BudgetController
	 * @param dataManager
	 */
	public BudgetController(DataManager dataManager)
	{
		this.dataManager = dataManager;
		
		addBudgetDialog = new AddBudgetDialog();
		addBudgetDialog.setCategories(dataManager.getExpenseCategories());;
		
		editBudgetDialog = new EditBudgetDialog();
		editBudgetDialog.setCategories(dataManager.getExpenseCategories());
		
		setupListeners();
		loadBudgets();
	}
	
	/**
	 * Register event handler functions.
	 */
	private void setupListeners()
	{
		//
		// DataManager - valueChanged
		//
		dataManager.addTransactionListener(new TransactionChangedListener() {
			public void valueChanged(TransactionChangedEvent event){
				transManager_valueChanged(event);
			}
		});
		//
		// budgetListView - clickPeformed
		//
		budgetListView.addClickListener(new BudgetItemClickListener() {
			public void clickPeformed(BudgetItemClickEvent event) {
				budgetListView_clickPeformed(event);
	 	 	}
		});
		//
		//	btn_Add - actionPerformed
		//
		btn_Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btn_Add_ActionPerformed(event);
			}
		});	
	}

	/**
	 * Handles the TransactionManager`s event that occurred.
	 * @param event - the TransactionEvent that occurred.
	 */
	private void transManager_valueChanged(TransactionChangedEvent event)
	{
		// Recalculate the budgets
		dataManager.recalculateBudgets();
				
		// update the BudgeListView Component
		budgetListView.refresh();
	}
	
	/**
	 * Handles the BudgetListView`s ClickEvent that occurred.
	 * @param event - The ClickEvent that occurred.
	 */
	private void budgetListView_clickPeformed(BudgetItemClickEvent event)
	{
		Budget budget = event.getBudget();
		
		// reset dialog box values
		editBudgetDialog.resetValues();
		
		editBudgetDialog.setName(budget.getName());
		editBudgetDialog.setValue(budget.getValue());
		editBudgetDialog.setNote(budget.getNote());
		editBudgetDialog.setStartDate(budget.getStartDate());
		editBudgetDialog.setEndDate(budget.getEndDate());
		editBudgetDialog.setSelectedCategories(budget.getCategories());
		editBudgetDialog.setTransactions(dataManager.getTransactionOfBudget(budget));
		
		// display the dialog 
		int dialogResult = editBudgetDialog.showDialog(this);
		
		// delete the budget
		if (dialogResult == EditBudgetDialog.DELETE)
		{
			budgetListView.removeBudget(budget);
			
			dataManager.removeBudget(budget.getID());
		}
		
		// update the budget
		if (dialogResult == EditBudgetDialog.SAVE)
		{
			dataManager.updateBudget(budget.getID(), 
					editBudgetDialog.getName(),
					editBudgetDialog.getValue(), 
					editBudgetDialog.getNote(), 
					editBudgetDialog.getStartDate(), 
					editBudgetDialog.getEndDate(),
					editBudgetDialog.getSelectedCategories());
			
			
			budgetListView.refresh();
		}
	}
	
	/**
	 * Handles the Add buttons ActionEvent that occurred.
	 * @param event - the ActionEvent that occurred.
	 */
	private void btn_Add_ActionPerformed(ActionEvent event)
	{
		addBudget();
	}

	/**
	 * Load all the budgets in to the BudgetListView component.
	 */
	private void loadBudgets()
	{
		for (int i = 0; i < dataManager.getBudgetCount(); i++ )
		{
			budgetListView.addBudget(dataManager.getBudget(i));
		}
		
		// recalculate the budgets
		dataManager.recalculateBudgets();
				
		// update the BudgeView Component
		budgetListView.refresh();
	}
		
	/**
	 * Create a new budget.
	 */ 
	private void addBudget()
	{
		// reset the dialog box
		addBudgetDialog.resetValues();
		
		// display the dialog box
		if (this.addBudgetDialog.showDialog(this))
		{
			// create a new budget
			int id = dataManager.addBudget(addBudgetDialog.getName(),
						addBudgetDialog.getValue(), 
						addBudgetDialog.getNote(), 
						addBudgetDialog.getStartDate(), 
						addBudgetDialog.getEndDate(),
						addBudgetDialog.getSelectedCategories());
			
			// calculate how much the user spent
			dataManager.recalculateBudget(dataManager.getBudgetbyID(id));
			
			// add the budget to the BudgetListView
			budgetListView.addBudget(dataManager.getBudgetbyID(id));
		}
	}
	
}
	
