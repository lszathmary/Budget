package gui.budgetPanel.addBudgetDialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import dataManager.Category;
import gui.timelinePanel.timelineTable.CategoryTableCellRenderer;

/**
 * BudgetDialog class
 * 	 The dialog used for adding/creating a new budget.
 */
@SuppressWarnings("serial")
public class AddBudgetDialog extends JDialog 
{
	private JTextField textFieldName;					// 
	private	JFormattedTextField textFieldValue;			// TextFields
	private JTextArea textPaneNote;						//
		
	protected JDatePanelImpl datePanelStartDate;		// 
	protected JDatePickerImpl datePickerStartDate;		// DatePickers
	protected JDatePanelImpl datePanelEndDate;			//	
	protected JDatePickerImpl datePickerEndDate;		//
	
	private JButton btnOk;								// Buttons
	private JButton btnCancel;							//
	
	private JTable tableCategory;						// Table
	private CategoryTableModel tableModel;				// TableModel
	
	private boolean dialogResult;	// True if the user clicked the "OK" button, otherwise false.
	
	/**
	 * Constructor
	 */
	public AddBudgetDialog() 
	{
		 initialize();
		 setupListeners();
		 resetValues();
		 
		 
	}
	
	/**
	 * Initializes the components of the dialog.
	 */
	private void initialize()
	{
		//
		// JDialog
		//
		setSize(new Dimension(570, 422));
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Budget");
		getContentPane().setLayout(new BorderLayout(0, 0));
		//
		// panelMain
		//
		JPanel panelMain = new JPanel();
		panelMain.setBorder(new EmptyBorder(0, 10, 0, 10));
		getContentPane().add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(null);
		//
		// lblName
		//
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(34, 11, 89, 14);
		panelMain.add(lblName);
		//
		// textFieldName
		//
		textFieldName = new JTextField(20);
		textFieldName.setBounds(34, 36, 149, 26);
		panelMain.add(textFieldName);
		textFieldName.setColumns(10);
		//
		// lblValue
		//
		JLabel lblValue = new JLabel("Amount:");
		lblValue.setBounds(34, 73, 89, 14);
		panelMain.add(lblValue);
		//
		// textFieldValue
		//
		// set the number of fraction digits to 2 so the number 
		// looks like this: 3.##, 1.01, 3.51 etc.
		// java horror stories :)
		textFieldValue = new  JFormattedTextField();
		DecimalFormat decimalformat = new DecimalFormat();
		decimalformat.setMinimumFractionDigits(2);
		decimalformat.setMaximumFractionDigits(2);
		NumberFormatter numberformmatter = new NumberFormatter(decimalformat);
		numberformmatter.setValueClass(BigDecimal.class);
		DefaultFormatterFactory factory = new DefaultFormatterFactory(numberformmatter);
		textFieldValue.setFormatterFactory(factory);
		textFieldValue.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldValue.setBounds(34, 98, 149, 26);
		panelMain.add(textFieldValue);
		//
		//  scrollPane
		//
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(209, 36, 320, 304);
		panelMain.add(scrollPane);
		//
		// lblCategory
		//
		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setBounds(209, 11, 101, 14);
		panelMain.add(lblCategory);
		//
		// tableCategory
		//
		tableCategory = new JTable();
		tableCategory.setBorder(null);
		scrollPane.setViewportView(tableCategory);
		tableModel = new  CategoryTableModel();
		tableCategory.setModel(tableModel);
		tableCategory.setTableHeader(null);
		tableCategory.setDefaultRenderer(Category.class, new CategoryTableCellRenderer());
		tableCategory.setRowHeight(36);
		this.tableCategory.getColumnModel().getColumn(0).setPreferredWidth(180);
		this.tableCategory.getColumnModel().getColumn(1).setPreferredWidth(25);
		//
		// lbStartDate
		//
		JLabel lbStartDate = new JLabel("Start Date:");
		lbStartDate.setBounds(34, 135, 89, 14);
		panelMain.add(lbStartDate);
		//
		// datePanelStartDate
		//
		UtilDateModel model1 = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		datePanelStartDate = new JDatePanelImpl(model1,p);
		datePickerStartDate = new JDatePickerImpl(datePanelStartDate, new DateLabelFormatter());
		datePickerStartDate.setBounds(34, 160, 149, 24);
		datePickerStartDate.getComponent(0).setPreferredSize(new Dimension(150,24));
		datePickerStartDate.getComponent(1).setPreferredSize(new Dimension(30,24));
		panelMain.add(datePickerStartDate);
		//
		//  lblEndDate
		//
		JLabel lblEndDate = new JLabel("End Date:");
		lblEndDate.setBounds(34, 195, 89, 14);
		panelMain.add(lblEndDate);
		//
		// datePicker_EndDate
		//
		UtilDateModel model2 = new UtilDateModel();
		Properties p2 = new Properties();
		p2.put("text.today", "Today");
		p2.put("text.month", "Month");
		p2.put("text.year", "Year");
		datePanelEndDate = new JDatePanelImpl(model2,p2);
		datePickerEndDate = new JDatePickerImpl(datePanelEndDate, new DateLabelFormatter());
		datePickerEndDate.setBounds(34, 220, 149, 24);
		datePickerEndDate.getComponent(0).setPreferredSize(new Dimension(150,24));
		datePickerEndDate.getComponent(1).setPreferredSize(new Dimension(30,24));
		panelMain.add(datePickerEndDate);
		//
		//	lblNote 
		//
		JLabel lblNote = new JLabel("Note:");
		lblNote.setBounds(34, 255, 89, 14);
		panelMain.add(lblNote);
		//
		// 	textPaneNote
		//
		textPaneNote = new JTextArea();
		textPaneNote.setBounds(34, 280, 149, 60);
		textPaneNote.setLineWrap(true);
		textPaneNote.setBorder(UIManager.getBorder("TextField.border"));
		panelMain.add(textPaneNote);
		//
		// panelBottom
		//
		JPanel panelBottom = new JPanel();
		FlowLayout fl_panelBottom = (FlowLayout) panelBottom.getLayout();
		fl_panelBottom.setHgap(30);
		fl_panelBottom.setAlignment(FlowLayout.RIGHT);
		panelBottom.setBorder(new EmptyBorder(0, 0, 10, 10));
		getContentPane().add(panelBottom, BorderLayout.SOUTH);
		//
		// btnOk
		//
		btnOk = new JButton("Add");
		btnOk.setPreferredSize(new Dimension(100, 26));
		panelBottom.add(btnOk);
		//
		// btnCancel
		//
		btnCancel = new JButton("Cancel");
		btnCancel.setPreferredSize(new Dimension(100, 26));
		panelBottom.add(btnCancel);
	}
	
	/**
	 * Register the event listeners.
	 */
	public void setupListeners()
	{
		//
		// 	buttonCancel - actionPerformed
		//
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnCancel_actionPerformed(arg0);
			}
		});
		//
		// buttonOk - actionPerformed
		//
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				btnOk_actionPerformed(arg0);
			}
		});
	}
		
	/**
	 * Handles the "OK" button`s ActionEvent that occurred.
	 * @param event - The ActionEvent that occurred.
	 */
	private void btnOk_actionPerformed(ActionEvent event)
	{
		if (this.getName().equals(""))
		{
			JOptionPane.showMessageDialog(this, " Please enter a name for the budget ! ");
			
			return;
		}
		
		if (this.getName().length() > 30)
		{
			JOptionPane.showMessageDialog(this, " The name of the budget must not be longer" +
					"than 30 charachters !" );
			
			return;
		}
		
		if (getStartDate().compareTo(getEndDate()) > 0 )
		{
			JOptionPane.showMessageDialog(this,
					"The start date must not be greater than the end date !");
			
			return;
		}
		
		if (this.getValue().compareTo(new BigDecimal(0)) <= 0 )
		{
			JOptionPane.showMessageDialog(this,
					"The budgets value must be greater than 0 !");
			
			return;
		}
		
		if (this.getSelectedCategories().size() == 0 )
		{
			JOptionPane.showMessageDialog(this,
					"You must select at least one category !");
			
			return;
		}
		
		dialogResult = true;
		setVisible(false);
	}
	
	/**
	 * Handles the "Cancel" button`s ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnCancel_actionPerformed(ActionEvent event)
	{
		this.dialogResult = false;
		this.setVisible(false);
	}
	
	/**
	 * Set the name displayed in the dialog box.
	 * @param name - The name to display in the dialog box.
	 */
	public void setName(String name)
	{
		this.textFieldName.setText(name);
	}
	
	/**
	 * Gets the name displayed in the dialog box.
	 * @return - The name that is displayed in the dialog. 
	 */
	public String getName()
	{
		return this.textFieldName.getText().trim();
	}
	
	/**
	 * Sets the value that is displayed in the dialog box.
	 * @param value - The value that is displayed in the dialog.
	 */
	public void setValue(BigDecimal value)
	{
		this.textFieldValue.setValue(value);
	}
	
	/**
	 * Gets the value that is displayed in the dialog box.
	 * @return - The value that is displayed in the dialog box.
	 */
	public BigDecimal getValue()
	{
		BigDecimal value = (BigDecimal)textFieldValue.getValue();
		
		// set scale to 2 so the number look like this #.##
		return value.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}
	
	/**
	 * Sets the start date that will be displayed in the dialog box.
	 * @param date - The start date to display in the dialog box. 
	 */
	public void setStartDate(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		datePanelStartDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		datePanelStartDate.getModel().setSelected(true);
	}
		
	/**
	 * Gets the start date that is displayed in the dialog box.
	 * @return - The date that is displayed in the dialog box. 
	 */
	public Date getStartDate()
	{
		return (Date) datePickerStartDate.getModel().getValue();
	}
	
	/**
	 * Sets the end date that will be displayed in the dialog box.
	 * @param date - The date that is displayed in the dialog box. 
	 */
	public void setEndDate(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		datePanelEndDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		datePanelEndDate.getModel().setSelected(true);
	}
	
	/**
	 * Gets the end date that is displayed in the dialog box.  
	 */
	public Date getEndDate()
	{
		return (Date) datePickerEndDate.getModel().getValue();
	}
	
	/**
	 * Sets the note that is displayed in the dialog
	 * @param note - The note to display.
	 */
	public void setNote(String note)
	{
		this.textPaneNote.setText(note);
	}
	
	/**
	 * Gets the note that is display in the dialog 
	 * @return the note that is displayed.
	 */
	public String getNote()
	{
		return this.textPaneNote.getText();
	}
	
	/**
	 * Set the selected categories, the other categories will be deselected. 
	 * @param categories - elected categories.
	 */
	public void setSelectedCategories(List<Category> categories)
	{
		tableModel.deselectAll();
		
		for (int i=0 ; i< categories.size(); i++)
		{
			this.tableModel.setSelected(categories.get(i), true);
		}
	}
	
	/**
	 * Gets the list of the categories that are selected.
	 * @return - selected categories.
	 */
	public ArrayList<Category> getSelectedCategories()
	{
		return this.tableModel.getSelected();
	}
	
	/**
	 * Set the list of the categories that the user can select from.
	 * @param categories - list of available categories.
	 */
	public void setCategories(List<Category> categories)
	{
		this.tableModel.RemoveAllRows();
		
		for (int i=0 ; i< categories.size(); i++)
		{
			this.tableModel.addRows(categories.get(i));
		}
	}
	
	/**
	 * Reset the values of the transaction to default which are displayed in the dialog box. 
	 */
	public void resetValues()
	{
		tableModel.deselectAll();
		
		this.setName("");
		this.setValue(new BigDecimal(0));
		this.setNote("");
		this.setStartDate(new Date());
		this.setEndDate(new Date());
		
		dialogResult = false;
	}


	/**
	 * Displays the dialog. Sets the location of the window relative to the 
	 * specified component. 
	 * @return - returns true if the user clicked the "OK" button, otherwise return false.
	 */
	public boolean showDialog(Component component)
	{
		dialogResult = false;
		
	 	setLocationRelativeTo(component);
	 	setVisible(true);
	 	
	 	return dialogResult;
	}
	
	/**
	 * Handle the conversion both from an Object to a String, and back from a 
	 * String to an Object. Needed for DatePicker.
	 */
	private class DateLabelFormatter extends AbstractFormatter 
	{
	    private String datePattern = "yyyy-MM-dd";
	    private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);
	    
	    /**
	     *  Converts date from String.
	     */
	    public Object stringToValue(String text) throws ParseException 
	    {
	        return dateFormatter.parseObject(text);
	    }

	    /**
	     * Converts date to String. 
	     */
	    public String valueToString(Object value) throws ParseException 
	    {
	        if (value != null) 
	        {
	            Calendar calendar = (Calendar) value;
	            return dateFormatter.format(calendar.getTime());
	        }

	        return "";
	    }
	}
}
