package gui.budgetPanel.addBudgetDialog;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.table.AbstractTableModel;

import dataManager.Category;


/**
 * CategoryTableModel class
 */
@SuppressWarnings("serial")
public class CategoryTableModel extends AbstractTableModel 
{
	// List of columns in the TableModle
	private String[] columnNames = {"Name", "Select" };

	// List of rows in the table model
	private ArrayList<Category> rowList = new ArrayList<>();

	// Map of category names, if the boolean is true it means the category is selected.
	private HashMap<String,Boolean> rowMap = new HashMap<String,Boolean>();
	
	/**
	 * Add a new row to the table model.
	 */
	public void addRows(Category category) 
	{
		rowMap.put(category.getName(), false);
		rowList.add(category);
		
		this.fireTableDataChanged();
    }
	
	/**
	 * Remove all the rows from the table model.
	 */
	public void RemoveAllRows()
	{
		this.rowMap.clear();
		this.rowList.removeAll(rowList);
		
		this.fireTableDataChanged();
	}
		
	/**
	 * Select/deselect the specified category. 
	 */
	public void setSelected(Category category, Boolean selected)
	{
		this.rowMap.put(category.getName(), selected);
			
		this.fireTableDataChanged();
	}
	
	/**
	 * Returns the list of selected categories.
	 */
	public ArrayList<Category> getSelected()
	{
		ArrayList<Category> categories = new ArrayList<Category>();
		
		for ( int i=0; i<rowList.size(); i++ )	
		{
			if (rowMap.get(rowList.get(i).getName()))
			{
				categories.add(rowList.get(i));
			}
		}
		
		return categories;
	}
	
	/**
	 * Deselect all of the categories in the table model
	 */
	public void deselectAll()
	{
		for (HashMap.Entry<String, Boolean> entry : rowMap.entrySet()) 
		{
		    entry.setValue(false);
		}
	}
	
	@Override
	public int getColumnCount() 
	{
		return this.columnNames.length;
		
	}

	@Override
	public int getRowCount() 
	{
		return this.rowList.size();
		
	}
	
	@Override
	public String getColumnName(int columnIndex) 
	{
		return columnNames[columnIndex];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Category row = rowList.get(rowIndex);
		Object value = null;
		
		switch (columnIndex) 
        {
        	 case 0:
        		 value = row;
        		 break;
        	 case 1:
        		 value = rowMap.get(row.getName());
        		 break;
        }
		
		return value;
	}
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) 
	{
		if (columnIndex == 1)
		{
			return true;
		}
		
		return false;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) 
	{
		Category row = rowList.get(rowIndex);
		
		if (columnIndex == 1)
		{
			rowMap.put(row.getName(),(boolean)aValue); 
		}
	}
	
	 @Override
	 public Class<?> getColumnClass(int column) 
	 {
	      switch (column) 
	      {
	          case 0:
	              return Category.class;
	              
	          case 1:
	              return Boolean.class;
	              
	          default:    
	          return Object.class;
	      }
	  }
	
}
