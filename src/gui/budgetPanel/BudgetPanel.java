package gui.budgetPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import gui.budgetPanel.budgetListView.BudgetListView;

/**
 * BudgetPanel class
 *	Represents the Budgets tab in the main application window.
 */
@SuppressWarnings("serial")
public class BudgetPanel extends JComponent
{
	protected JButton btn_Add;					// Button with the "+" label. 
	protected BudgetListView budgetListView;	// Custom component
	
	/**
	 * Creates the BudgetPanel.
	 */
	public BudgetPanel() 
	{
		this.Initialize();
	}
	
	/**
	 * Initialize a new instance the components of the BudgetPanel
	 */
	public void Initialize()
	{
		setLayout(new BorderLayout(0, 0));
		//
		// panel_Main
		//
		JPanel panel_Main = new JPanel();
		panel_Main.setBorder(new EmptyBorder(20, 20, 10, 20));
		add(panel_Main, BorderLayout.CENTER);
		panel_Main.setLayout(new BorderLayout(0, 0));
		//
		// panel_Top
		//
		JPanel panel_Top = new JPanel();
		panel_Main.add(panel_Top, BorderLayout.NORTH);
		//
		// panel_Middle
		//
		JPanel panel_Middle = new JPanel();
		panel_Main.add(panel_Middle, BorderLayout.CENTER);
		panel_Middle.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel_Middle.setLayout(new BoxLayout(panel_Middle, BoxLayout.Y_AXIS));
		//
		// scrollPane
		//
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		panel_Middle.add(scrollPane);
		//
		// budgetView
		//
		budgetListView = new BudgetListView();
		budgetListView.setBorder(null);
		scrollPane.setViewportView(budgetListView);
		budgetListView.setLayout(new BoxLayout(budgetListView, BoxLayout.Y_AXIS));
		//
		// panel_Bottom
		// 
   	 	JPanel panel_Bottom = new JPanel();
   	 	panel_Bottom.setBorder(new EmptyBorder(10, 0, 0, 0));
		panel_Main.add(panel_Bottom, BorderLayout.SOUTH);
		//
		// btn_Add
		// 
		btn_Add = new JButton("+");
		btn_Add.setMinimumSize(new Dimension(70,26));
		btn_Add.setPreferredSize(new Dimension(70,26));
		panel_Bottom.add(btn_Add);
	}
	
}
