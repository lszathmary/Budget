package gui.timelinePanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import dataManager.Category;
import gui.timelinePanel.timelineTable.BigDecimalTableCellRenderer;
import gui.timelinePanel.timelineTable.CategoryTableCellRenderer;
import gui.timelinePanel.timelineTable.DateTableCellRenderer;
import gui.timelinePanel.timelineTable.StringTableCellRenderer;
import gui.timelinePanel.timelineTable.TimeLineTableModel;
import gui.timelinePanel.timelineTable.TimelineTableRowSorter;

/**
 * TimelinePanel class
 * 	Represents the "TimeLine" tab in the main application window.
 */
@SuppressWarnings("serial")
public class TimeLinePanel extends Panel
{
	protected JTable tableTimeLine;
	protected TimeLineTableModel  timeLineTableModel;
	
	protected JButton btnAdd;
	protected JButton btnEdit;
	protected JButton btnRemove;
	protected JButton btnHelp;
	
	protected JComboBox<String> comboBoxPeriod;
	
	protected JDatePanelImpl datePanelStartDate;
	protected JDatePickerImpl datePickerStartDate;
	
	protected JDatePanelImpl datePanelEndDate;
	protected JDatePickerImpl datePickerEndDate;
	
	/**
	 * Create the "TimeLine" panel. 
	 */
	public TimeLinePanel() 
	{
		//
		// panel
		//
		setSize(new Dimension(750,400));
		setLayout(new BorderLayout(0, 0));
		//
		// datePickerStartDate
		//
		UtilDateModel model = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		datePanelStartDate = new JDatePanelImpl(model,p);
		//
		// datePickerEndDate
		//
		UtilDateModel model2 = new UtilDateModel();
		Properties p2 = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		datePanelEndDate = new JDatePanelImpl(model2,p2);
		//
		// panelLeft 
		//
		JPanel panelLeft = new JPanel();
		panelLeft.setLayout(new BorderLayout(0, 0));
		this.add(panelLeft);
		//
		// panelMiddle
		//
		JPanel panelMiddle = new JPanel();
		panelMiddle.setBorder(new EmptyBorder(10, 20, 10, 20));
		panelLeft.add(panelMiddle, BorderLayout.CENTER);
		panelMiddle.setLayout(new BoxLayout(panelMiddle, BoxLayout.X_AXIS));
		//
		// scrollPane
		//
		JScrollPane scrollPane = new JScrollPane();
		panelMiddle.add(scrollPane);
		//
		// table
		//
		timeLineTableModel = new TimeLineTableModel();
		tableTimeLine = new JTable(timeLineTableModel );
		tableTimeLine.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableTimeLine.setDefaultRenderer(Category.class, new CategoryTableCellRenderer());
		tableTimeLine.setDefaultRenderer(BigDecimal.class, new BigDecimalTableCellRenderer());
		tableTimeLine.setDefaultRenderer(Date.class, new DateTableCellRenderer());
		tableTimeLine.setDefaultRenderer(String.class, new StringTableCellRenderer());
		tableTimeLine.setAutoCreateRowSorter(true);
		tableTimeLine.setRowSorter(new  TimelineTableRowSorter(timeLineTableModel));
		tableTimeLine.setRowHeight(36);
		scrollPane.setViewportView(tableTimeLine);
		//
		// panelTop
		//
		JPanel panelTop = new JPanel();
		panelLeft.add(panelTop, BorderLayout.NORTH);
		panelTop.setBorder(new EmptyBorder(10, 20, 10, 20));
		FlowLayout fl_panelTop = new FlowLayout(FlowLayout.CENTER, 5, 5);
		fl_panelTop.setAlignOnBaseline(true);
		panelTop.setLayout(fl_panelTop);
		//
		// labelPeriod
		//
		JLabel lblPeriod = new JLabel("Period: ");
		panelTop.add(lblPeriod);
		//
		// comboBoxPeriod
		//
		DefaultComboBoxModel<String> modelPeriod = new DefaultComboBoxModel<String>();
		modelPeriod.addElement("All");
		modelPeriod.addElement("Today");
		modelPeriod.addElement("Yesterday");
		modelPeriod.addElement("This Week");
		modelPeriod.addElement("Last Week");
		modelPeriod.addElement("This Month");
		modelPeriod.addElement("Last Month");
		modelPeriod.addElement("This Year");
		modelPeriod.addElement("Custom");
		comboBoxPeriod = new JComboBox<String>();
		comboBoxPeriod.setModel(modelPeriod);
		comboBoxPeriod.setLightWeightPopupEnabled(false);
		comboBoxPeriod.setPreferredSize(new Dimension(110, 26));
		panelTop.add(comboBoxPeriod);
		//
		// lblStartDate
		//
		JLabel lblStartDate = new JLabel("   Start Date: ");
		panelTop.add(lblStartDate);
		datePickerStartDate = new JDatePickerImpl(datePanelStartDate, new DateLabelFormatter());
		datePickerStartDate.setPreferredSize(new Dimension(160, 28));
		datePickerStartDate.getComponent(0).setPreferredSize(new Dimension(100,28));
		datePickerStartDate.getComponent(1).setPreferredSize(new Dimension(30,28));
		panelTop.add(datePickerStartDate);
		//
		// lblEndDate
		//
		JLabel lblEndDate = new JLabel("   End Date:");
		panelTop.add(lblEndDate);
		datePickerEndDate = new JDatePickerImpl(datePanelEndDate, new DateLabelFormatter());
		datePickerEndDate.setPreferredSize(new Dimension(160, 28));
		datePickerEndDate.getComponent(0).setPreferredSize(new Dimension(100,28));
		datePickerEndDate.getComponent(1).setPreferredSize(new Dimension(30,28));
		panelTop.add(datePickerEndDate);
		//
		// panelBottom
		//
		JPanel panelBottom = new JPanel();
		panelBottom.setBorder(new EmptyBorder(0, 20, 10, 20));
		panelLeft.add(panelBottom, BorderLayout.SOUTH);
		panelBottom.setLayout(new BorderLayout(0, 0));
		//
		// panelBottomCenter
		//
		JPanel panelBottomCenter = new JPanel();
		panelBottom.add(panelBottomCenter, BorderLayout.CENTER);
		panelBottomCenter.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		//
		// btnAdd
		//
		btnAdd = new JButton("Add");
		btnAdd.setPreferredSize(new Dimension(100, 26));
		panelBottomCenter.add(btnAdd);
		//
		// btnEdit
		//
		btnEdit = new JButton("Edit");
		btnEdit.setPreferredSize(new Dimension(100, 26));
		btnEdit.setEnabled(false);
		panelBottomCenter.add(btnEdit);
		//
		// btnRemove
		//
		btnRemove = new JButton("Remove");
		btnRemove.setPreferredSize(new Dimension(100, 26));
		btnRemove.setEnabled(false);
		panelBottomCenter.add(btnRemove);
		//
		// panelBottomLeft
		//
		JPanel panelBottomLeft = new JPanel();
		panelBottom.add(panelBottomLeft, BorderLayout.LINE_END);
		//
		// btnHelp
		//
		btnHelp = new JButton("Balance");
		btnHelp.setPreferredSize(new Dimension(100, 26));
		//panelBottomLeft.add(btnHelp);
	}


	/**
	 * Set the the date of "Start Date" date picker component.
	 * @param date - The new date of the "Start Date" date picker component.
	 */
	protected void setStartDate(Date date)
	{
		if (date == null) 
		{
			datePanelStartDate.getModel().setValue(null);
			
			return;
		}
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
						
		datePanelStartDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), 
				calendar.get(Calendar.DAY_OF_MONTH));
		
		datePanelStartDate.getModel().setSelected(true);
	}
	
	/**
	 * Get the the date of "Start Date" date picker component.
	 * @return
	 */
	protected Date getStartDate()
	{
		return (Date) datePickerStartDate.getModel().getValue();
	}
	
	/**
	 * Set the the date of "End Date" date picker component.
	 * @param date
	 */
	protected void setEndDate(Date date)
	{
		if (date == null) 
		{
			datePanelEndDate.getModel().setValue(null);
			
			return;
		}
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
	
		datePanelEndDate.getModel().setDate(calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), 
				calendar.get(Calendar.DAY_OF_MONTH));
		
		datePanelEndDate.getModel().setSelected(true);
	}
	
	/**
	 * Get the the date of "End Date" date picker component.
	 * @return
	 */
	protected Date getEndDate()
	{
		return (Date) datePickerEndDate.getModel().getValue();
	}

	/**
	 * Enables or disables "Start Date" and "End Date" date picker components, depending on the 
	 * value of the parameter.  
	 */
	protected void setEnabledDateChange(boolean bool)
	{
		datePickerStartDate.getComponent(1).setEnabled(bool);
		datePickerEndDate.getComponent(1).setEnabled(bool);
	}
	
	/**
	 * Handle the conversion both from an Object to a String, and back from a 
	 * String to an Object. Needed for DatePicker.
	 */
	private class DateLabelFormatter extends AbstractFormatter 
	{
	    private String datePattern = "  dd MMMM yyyy";
	    private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);
	    
	    public Object stringToValue(String text) throws ParseException 
	    {
	        return dateFormatter.parseObject(text);
	    }

	    public String valueToString(Object value) throws ParseException 
	    {
	        if (value != null) 
	        {
	            Calendar calendar = (Calendar) value;
	            return dateFormatter.format(calendar.getTime());
	        }

	        return "";
	    }
	}
	
}
