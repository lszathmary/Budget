package gui.timelinePanel.timelineTable;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import dataManager.Category;
import dataManager.Transaction;

/**
 * BigDecimalTableCellRenderer class
 * 	Responsible for rendering the BigDecimal classes in a JTable.
 */
@SuppressWarnings("serial")
public class BigDecimalTableCellRenderer extends DefaultTableCellRenderer
{
	public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
	{
		this.setHorizontalAlignment(RIGHT);

		// row index after rows are sorted
		int rowIndex = table.convertRowIndexToModel(row);
		
		Transaction transaction = ((TimeLineTableModel)table.getModel()).getRow(rowIndex);
	
		//table.getsor
		
		if (transaction.getType() == Category.Type.Expense )
		{
			setText("- " + value.toString() + "  ");
		}
		
		if (transaction.getType() == Category.Type.Income )
		{
			setText(value.toString() + "  ");
		}
			
		if (isSelected) 
        {
    		setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } 
        else 
        {
        	setBackground(table.getBackground());
            setForeground(table.getForeground());
        }
		
		
		return this;
	}
}
