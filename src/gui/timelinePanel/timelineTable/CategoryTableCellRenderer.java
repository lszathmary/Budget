package gui.timelinePanel.timelineTable;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import dataManager.Category;

/**
 * CategoryTableCellRender class
 * 	Responsible for rendering the Category classes in a JTable.
 */
@SuppressWarnings("serial")
public class CategoryTableCellRenderer extends DefaultTableCellRenderer
{
	public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
	{
		Category category = (Category) value;

		setIcon(category.getIcon());
		setText(" " + category.getName());
		setIconTextGap(5);
		
		if (isSelected) 
        {
    		setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } 
        else 
        {
        	setBackground(table.getBackground());
            setForeground(table.getForeground());
        }
		
		return this;
	}
}
