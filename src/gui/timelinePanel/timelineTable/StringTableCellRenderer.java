package gui.timelinePanel.timelineTable;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * StringTableCellRenderer class
 * 	Responsible for rendering the String classes in a JTable.
 */
@SuppressWarnings("serial")
public class StringTableCellRenderer extends DefaultTableCellRenderer
{
	public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
	{
		setText(" " + value.toString());
		
		if (isSelected) 
        {
    		setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } 
        else 
        {
        	setBackground(table.getBackground());
            setForeground(table.getForeground());
        }
		
		return this;
	}
}
