package gui.timelinePanel.timelineTable;

import java.util.List;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

import dataManager.Transaction;


@SuppressWarnings("serial")
public class TimeLineTableModel extends AbstractTableModel
{
	// List of columns in the TableModle
	private String[] columnNames = {"ID", "Category", "Value", "Tag", "Date", ""};

	// List of rows stored in the TableModle
	private List<Transaction> rows = new ArrayList<>();
	
	// Am empty string for the dummy column.
	private String emptyString = "";

	/**
	 * Add a row to the table model. 
	 */
	public void addRow(Transaction transaction) 
	{
		this.rows.add(transaction);
				
		this.fireTableDataChanged();
    }
	
	/**
	 * Remove a row from the table model. 
	 */
	public void removeRow(Transaction transaction)
	{
		this.rows.remove(transaction);
		
		this.fireTableDataChanged();
	}
	
	/**
	 * Remove all the rows from the table model.
	 */
	public void removeAllRows()
	{
		this.rows.removeAll(rows);
		
		this.fireTableDataChanged();
	}
	
	/**
     * Return the row with the specified row index.
     */
    public Transaction getRow(int rowIndex)
    {
    	return rows.get(rowIndex);
    }
    
	
	/**
     * Returns the name of the column with the specified index. 
     */
	@Override
	public String getColumnName(int columnIndex) 
	{
		return columnNames[columnIndex];
	}
	
	/**
     * Returns the number of rows in the table model.
     */
	@Override
    public int getRowCount() 
	{
		return this.rows.size();
    }
	 
	/**
     * Returns the number of columns in the table model.
     */
	@Override
	public int getColumnCount()
	{
		return this.columnNames.length;
    }
	
	/**
     * Returns the most specific superclass for all the cell values in the column.
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) 
    {
    	if (this.rows.isEmpty())
    	{
    		return Object.class;
    	}
    	
        return getValueAt(0, columnIndex).getClass();
    }
	
    /**
     * Returns the value for the cell at columnIndex and rowIndex.
     */
    @Override
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
         Transaction row = rows.get(rowIndex);
         Object value = null;
         
         switch (columnIndex) 
         {
         	 case 0:
         		 value = row.getID();
         		 break;
             case 1:
                 value = row.getCategory();
                 break;
             case 2:
                 value = row.getValue();
                 break;
             case 3:
                 value = row.getTag();
                 break;
             case 4:
                 value = row.getDate();
                 break;
             case 5:
                 value = this.emptyString;
                 break;
         }
         
         return value;
    }
 
 }
