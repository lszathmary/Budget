package gui.timelinePanel.timelineTable;

import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;


/**
 * TimelineTableRowSorter class
 * 	Dummy table sorter for disabling sorting in the last dummy column. 
 */
public class TimelineTableRowSorter extends TableRowSorter<TableModel>
{
	public TimelineTableRowSorter(TableModel tableModel) 
	{
		super(tableModel);
    }

	@Override
    public boolean isSortable(int column) 
    {
        if (column == 5) return false;
        else return true;
    }
}


