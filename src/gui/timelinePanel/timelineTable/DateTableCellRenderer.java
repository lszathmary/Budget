package gui.timelinePanel.timelineTable;

import java.awt.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * DateTableCellRenderer r class
 * 	Responsible for rendering the Date classes in a JTable.
 */
@SuppressWarnings("serial")
public class DateTableCellRenderer extends DefaultTableCellRenderer 
{
	
	public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
	{
		Date date = (Date)value;
		
		DateFormat dateFormat = new SimpleDateFormat(" dd - MMM MM -yyyy");
		
		this.setHorizontalAlignment(CENTER);
		this.setText(dateFormat.format(date));
		
		if (isSelected) 
        {
    		setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } 
        else 
        {
        	setBackground(table.getBackground());
            setForeground(table.getForeground());
        }
		
		return this;
	}
}
