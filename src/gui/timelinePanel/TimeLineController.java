package gui.timelinePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dataManager.DataManager;
import dataManager.Transaction;
import gui.SummaryPanel.BalanceDialog.BalanceDialog;
import gui.timelinePanel.timelineTable.TimeLineTableModel;
import gui.timelinePanel.transactionDialog.TransactionDialog;
import utils.DateUtils;


/**
 * TimeLineController class
 * 	The TimeLineController is the coordinator for the "TimeLine" portion of my application. When 
 * 	the user works in the "TimeLine" tab of the application, the TimeLineController handles the 
 * 	interactions with the user, displaying dialogs, working with other controllers, and updating 
 * 	TimeLine related data.
 */
@SuppressWarnings("serial")
public class TimeLineController extends TimeLinePanel
{
	private DataManager dataManager;
	
	//
	private TransactionDialog transactionDialog;
	private BalanceDialog balanceDialog;
	
	/**
	 * Create the TimeLineController.
	 * @param dataManager - The reference to the DataManager class.
	 */
	public TimeLineController(DataManager dataManager)
	{
		this.dataManager = dataManager;
		
		// Color tool
		// System.out.println(new Color(145,89,237).getRGB());
		
		// setup the "Transaction" dialog
		balanceDialog = new BalanceDialog();
		transactionDialog = new TransactionDialog();
		
		transactionDialog.setExpenseCategories(dataManager.getExpenseCategories());
		transactionDialog.setIncomeCategories(dataManager.getIncomeCategories());
		
		setupListeners();
		setupTimeLineDataTable();
		setEnabledDateChange(false);
		refreshTimeLineTable();
	}

	
	/**
	 * Register event handler functions.
	 */
	private void setupListeners()
	{
		//
		// btnAdd - actionPerformed
		//
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnAdd_actionPeformed(arg0);
			}
		});
		//
		// btnEdit - actionPerformed
		//
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnEdit_actionPeformed(arg0);
			}
		});
		//
		// btnRemove - actionPerformed
		//
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnRemove_actionPeformed(arg0);
			}
		});	
		//
		// btnHelp - actionPerformed
		//
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnHelp_actionPeformed(arg0);
			}
		});	
		//
		//	comboBoxPeriod - actionPerformed
		//
		comboBoxPeriod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				comboBox_Period_actionPerformed(arg0);
			}
		});
		//
		// tableTimeLine - valueChanged
		//
		tableTimeLine.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent arg0){
				tableTimeLine_valueChanged(arg0);
			}
		});
		// 
		// datePicker_StartDate - actionPerformed
		//
		datePickerStartDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datePicker_StartDate_actionPerformed(arg0);
			}
		});
		//
		// datePicker_EndDate - actionPerformed
		//
		datePickerEndDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datePicker_EndDate_actionPerformed(arg0);
			}
		});
		
	}	
	
	/**
	 * Setup the "TimeLine" table;
	 */
	private void setupTimeLineDataTable()
	{
		tableTimeLine.getColumnModel().getColumn(0).setPreferredWidth(50);
		tableTimeLine.getColumnModel().getColumn(0).setMaxWidth(80);
				
		tableTimeLine.getColumnModel().getColumn(1).setPreferredWidth(230);
		tableTimeLine.getColumnModel().getColumn(1).setMaxWidth(330);
		
		tableTimeLine.getColumnModel().getColumn(2).setPreferredWidth(120);
		tableTimeLine.getColumnModel().getColumn(2).setMaxWidth(150);
		
		tableTimeLine.getColumnModel().getColumn(3).setPreferredWidth(180);
		
		tableTimeLine.getColumnModel().getColumn(4).setPreferredWidth(140);
		tableTimeLine.getColumnModel().getColumn(4).setMaxWidth(200);
		
		tableTimeLine.getColumnModel().getColumn(5).setPreferredWidth(380);
		
		tableTimeLine.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tableTimeLine.getRowSorter().toggleSortOrder(4);
	}

	/**
	 * Handles the "Add" button ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnAdd_actionPeformed(ActionEvent e)
	{
		this.addTransaction();
	}
	
	/**
	 * Handles the "Edit" button ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnEdit_actionPeformed(ActionEvent e)
	{
		this.editTransaction();
	}
	
	/**
	 * Handles the "Remove" button ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnRemove_actionPeformed(ActionEvent e)
	{
		this.removeTransaction();
	}
	
	/**
	 * Handles the "Help" button ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void btnHelp_actionPeformed(ActionEvent e)
	{
		balanceDialog.showDialog(this);
	}
	
	/**
	 * Handles the "comboBox_Period" ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void comboBox_Period_actionPerformed(ActionEvent e)
	{
		Date startDate = new Date();
		Date endDate = new Date();

		// display all the transaction in the "TimeLine" table
		if (comboBoxPeriod.getSelectedItem().toString() == "All")
		{
			this.setEnabledDateChange(false);
		}

		// display the transactions that happened today 
		if (comboBoxPeriod.getSelectedItem().toString() == "Today")
		{
			startDate = new Date();
			endDate = new Date();
			
			this.setEnabledDateChange(false);
		}

		// display the transactions that happened yesterday
		if (comboBoxPeriod.getSelectedItem().toString() == "Yesterday")
		{
			startDate = DateUtils.getYesterday();
			endDate = DateUtils.getYesterday();
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened this week
		if (comboBoxPeriod.getSelectedItem().toString() == "This Week")
		{
			startDate = DateUtils.getFirstDayOfWeek(new Date());
			endDate = DateUtils.getLastDayOfWeek(new Date());
			
			this.setEnabledDateChange(false);
		}

		// display the transactions that happened last week
		if (comboBoxPeriod.getSelectedItem().toString() == "Last Week")
		{
			startDate = DateUtils.getFirstDayOfWeek(DateUtils.getLastWeek());
			endDate = DateUtils.getLastDayOfWeek(DateUtils.getLastWeek());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened last week
		if (comboBoxPeriod.getSelectedItem().toString() == "Last Week")
		{
			startDate = DateUtils.getFirstDayOfWeek(DateUtils.getLastWeek());
			endDate = DateUtils.getLastDayOfWeek(DateUtils.getLastWeek());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened this month
		if (comboBoxPeriod.getSelectedItem().toString() == "This Month")
		{
			startDate = DateUtils.getFirstDayOfMonth(new Date());
			endDate = DateUtils.getLastDayOfMonth(new Date());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened last month
		if (comboBoxPeriod.getSelectedItem().toString() == "Last Month")
		{
			startDate = DateUtils.getFirstDayOfMonth(DateUtils.getLastMonth());
			endDate = DateUtils.getLastDayOfMonth(DateUtils.getLastMonth());
			
			this.setEnabledDateChange(false);
		}
		
		// display the transactions that happened this year
		if (comboBoxPeriod.getSelectedItem().toString() == "This Year")
		{
			startDate = DateUtils.getFirstDayOfYear(new Date());
			endDate = DateUtils.getLastDayOfYear(new Date());
			
			this.setEnabledDateChange(false);
		}
					
		// allow the user to select the period of the transactions
		if (comboBoxPeriod.getSelectedItem().toString() == "Custom")
		{
			startDate = this.getStartDate();
			endDate = this.getEndDate();
			
			this.setEnabledDateChange(true);
		}
		
		setStartDate(startDate);
		setEndDate(endDate);
		
		refreshTimeLineTable();
	}
		
	/**
	 * Handles the "TimeLine" data table ListSelectionEven that occurred.
	 * @param e - the ListSelectionEvent that occurred.
	 */
	private void tableTimeLine_valueChanged(ListSelectionEvent e)
	{
		// if there is nothing selected in the "TimeLine" DataTable disable the "Edit" and 
		// the "Remove" Buttons otherwise enable them.
		if (tableTimeLine.getSelectedRow() == -1)
		{
			btnEdit.setEnabled(false);
			btnRemove.setEnabled(false);
		}
		else
		{
			btnEdit.setEnabled(true);
			btnRemove.setEnabled(true);
		}
	}
	
	/**
	 * Handles the "Start Date" picker`s ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void datePicker_StartDate_actionPerformed(ActionEvent e)
	{
		this.loadTransactions(getStartDate(), getEndDate());
	}
	
	/**
	 * Handles the "End Date" picker`s ActionEvent that occurred.
	 * @param e - the ActionEvent that occurred.
	 */
	private void datePicker_EndDate_actionPerformed(ActionEvent e)
	{
		this.loadTransactions(getStartDate(), getEndDate());
	}
	
	/**
	 * add a new transaction.
	 */
	private void addTransaction()
	{
		// reset the values displayed by the "Transaction" dialog
		this.transactionDialog.resetValues();
		
		// display the "Transaction" dialog
		if (this.transactionDialog.showDialog(this))
		{
			// create a new transaction
			dataManager.addTransaction(transactionDialog.getCategory(), 
							transactionDialog.getValue(),
							transactionDialog.getTag(),
							transactionDialog.getDate());
			
			this.refreshTimeLineTable();			
		}
	}	
	
	/**
	 * Edit the transaction that is selected in the "TimeLine" table. 
	 */
	private void editTransaction()
	{
		// get the index of the currently selected row
		TimeLineTableModel model = (TimeLineTableModel)tableTimeLine.getModel();
		int rowIndex = tableTimeLine.convertRowIndexToModel(tableTimeLine.getSelectedRow());

		// get selected transaction
		Transaction transaction = model.getRow(rowIndex);

		// set the values displayed by the "Transaction" dialog
		transactionDialog.setID(transaction.getID());
		transactionDialog.setDate(transaction.getDate());
		transactionDialog.setCategory(transaction.getCategory());
		transactionDialog.setValue(transaction.getValue());
		transactionDialog.setTag(transaction.getTag());
		
		// display the "Transaction" dialog
		if (this.transactionDialog.showDialog(this))
		{
			dataManager.updateTransaction(transaction.getID(), 
					transactionDialog.getCategory(), 
					transactionDialog.getValue(),
					transactionDialog.getTag(),
					transactionDialog.getDate());
			
			this.refreshTimeLineTable();
		}
	}

	/**
	 * Remove the transaction that is selected in the "TimeLine" table. 
	 */
	private void removeTransaction()
	{
		// display confirm dialog
		int result = JOptionPane.showConfirmDialog(this,  
				"Are you sure you want to remove the selected transaction ?" ,
				"JBudget", 	JOptionPane.YES_NO_OPTION);
		
		// in case the user clicked the "OK" button
		if (result == JOptionPane.OK_OPTION)
		{
			// get the index of the currently selected row
			TimeLineTableModel model = (TimeLineTableModel)tableTimeLine.getModel();
			int rowIndex = tableTimeLine.convertRowIndexToModel(tableTimeLine.getSelectedRow());

			// get selected transaction
			Transaction transaction = model.getRow(rowIndex);
						
			// remove transaction from the manager
			dataManager.removeTransaction(transaction.getID());
			
			
			refreshTimeLineTable();
		}
	}
	
	/**
	 * Load in the "TimeLine" table the transactions that occurred in specified period. 
	 * @param startDate - The beginning of the period, including this day.
	 * @param endDate - The end of the period, including this day.
	 */
	private void loadTransactions(Date startDate, Date endDate)
	{
		TimeLineTableModel timeLineModel = (TimeLineTableModel)tableTimeLine.getModel();
		
		// clear the "TimeLine" table
		timeLineModel.removeAllRows();
		
		if ( (startDate == null) || (endDate == null))
		{
			return;
		}
	
		// get list of the transactions that happened in the specified period
		ArrayList<Transaction> transactions = dataManager.getTransactionsInPeriod(startDate,endDate);
		
		// add transactions to the "TimeLine" table
		for (int i=0; i< transactions.size(); i++)
		{
			timeLineModel.addRow(transactions.get(i));
		}
	}

	
	/**
	 * Refresh the data in "TimeLine" table.
	 */
	private void refreshTimeLineTable()
	{
		if (comboBoxPeriod.getSelectedItem().toString() == "All")
		{
			if (dataManager.getTransactionCount() == 0)
			{
				setStartDate(null);
				setEndDate(null);
			}
			else 
			{
				setStartDate(dataManager.getFirstTransaction().getDate());
				setEndDate(dataManager.getLastTransaction().getDate());
			}
		}
		
		loadTransactions(getStartDate(), getEndDate());
	}
}
