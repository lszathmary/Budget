package gui.timelinePanel.transactionDialog;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import dataManager.Category;


/**
 * CategoryListCellRenderer 
 *	Responsible for rendering the Category classes in a JComboBox.
 */
 
@SuppressWarnings("serial")
class CategoryListCellRenderer extends DefaultListCellRenderer 
{
    public Component getListCellRendererComponent(JList<?> list, Object value,
    		int index, boolean isSelected, boolean cellHasFocus) 
    {
    	Category category = (Category) value;
    	
    	setOpaque(true);
        setHorizontalAlignment(LEFT);
        setVerticalAlignment(CENTER);
        setIconTextGap(10);
        
    	if (isSelected) 
        {
    		setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } 
        else 
        {
        	setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

    	setIcon(category.getIcon());
        setText(category.getName());
        setFont(list.getFont());
        
        return this;
    }
   
}