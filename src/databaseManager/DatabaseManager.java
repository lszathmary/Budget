package databaseManager;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JOptionPane;

import utils.Convert;

/**
 * DatabaseManager class 
 * 	Responsible for reading and writing data to/from the database.
 */
public class DatabaseManager
{
	// a connection with a specific database
	Connection connection = null;

	/**
	 * Establish a connection to the database.
	 */
	public void Connect()
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			
			connection = DriverManager.getConnection("jdbc:sqlite:" + new File(".").getCanonicalPath() + "/data.sqlite");
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	/**
	 * Read all the categories from the database into a result set.
	 * @return - The result set containing the categories.
	 */
	public ResultSet loadCategories()
	{
		ResultSet resultSet = null;
		try
		{
			// read  categories from database
			String query = "SELECT * FROM T_Category";

			PreparedStatement pStatement = connection.prepareStatement(query);
			resultSet = pStatement.executeQuery();
		}
		catch(SQLException ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return resultSet;
	}
	
	/**
	 * Load all the transactions from the database into a result set.
	 * @return - result set containing the transactions.
	 */
	public ResultSet loadTransactions()
	{
		ResultSet resultSet = null;
		
		try
		{
			String query = "SELECT * FROM T_Transaction";
			
			PreparedStatement pStatement = connection.prepareStatement(query);
			resultSet = pStatement.executeQuery();
		}
		catch(SQLException ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return resultSet;
	}
	
	/**
	 * Insert a new transaction into the database. 
	 * @param category - the category of the transaction. 
	 * @param value - the value of the transaction.
	 * @param tag - the tag of the transaction.
	 * @param date - the date of the transaction.
	 * @return the id of the transaction.
	 */
	public int insertTransaction(String category, BigDecimal value, String tag, Date date)
	{
		int id = -1;
		
		String query = "INSERT INTO T_Transaction(Category, Value, Tag, Date) " +
					"VALUES ( ?, ?, ?, DATE(?) )";
		
		try
		{
			// insert transaction into the database
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			
			pStatement.setString(1,category);
			pStatement.setString(2, value.toString());
			pStatement.setString(3,tag);
			pStatement.setString(4,Convert.DateToString(date));
			pStatement.executeUpdate();
					
			// read the auto generated id from the database
			ResultSet resultSet = pStatement.getGeneratedKeys();
		
			while(resultSet.next())
			{
				id = resultSet.getInt(1);
			}
			
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return id;
	}
	
	/**
	* Update transaction information in the database.
	 * @param ID - the ID of the transaction that will be updated.
	 * @param category - the new category of the transaction.
	 * @param value - the new value of the transaction.
	 * @param tag - the new tag of the transaction.
	 * @param date - the new date of the transaction.
	 */
	public void updateTransaction(int ID, String category, BigDecimal value, String tag, Date date)
	{
		String query = "UPDATE T_Transaction SET Category='" + category + "'," +
				" Value=" + value.toString() + "," +
				" Tag='" + tag + "'," +
				" Date=" + "DATE('" + Convert.DateToString(date) + "')" + 
				" WHERE ID= " + Integer.toString(ID) +";";
				
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			pStatement.executeUpdate();
		
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
	}
	
	/**
	 * Remove transaction with specified ID from the database. 
	 * @param ID - the ID of the transaction that will be removed.
	 */
	public void removeTransaction(int ID)
	{
		String query = "DELETE FROM T_Transaction WHERE ID=" + Integer.toString(ID) +";";
		
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			pStatement.executeUpdate();
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
	}
	
	/**
	 * Gets the id of the oldest transaction from the database. 
	 * @return id of the oldest transaction
	 */
	public int getFirstTransactionID()
	{
		int id = -1;
		String query = "SELECT T_Transaction.ID, MIn(T_Transaction.Date) FROM T_Transaction";
				
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			ResultSet resultSet = pStatement.executeQuery();
			
			while(resultSet.next())
			{
				 id = resultSet.getInt("ID");
			}
		}
		catch(Exception ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return id;
	}
	
	/**
	 * Gets the id of the last transaction from the database.
	 * @return the most recent transaction in the database 
	 */
	public int getLastTransactionID()
	{
		int id = -1;
		String query = "SELECT T_Transaction.ID, MAX(T_Transaction.Date) FROM T_Transaction";
				
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			ResultSet resultSet = pStatement.executeQuery();
			
			while(resultSet.next())
			{
				 id = resultSet.getInt("ID");
			}
		}
		catch(Exception ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return id;
	}
	
	/**
	 * Load the transactions from the database that occurred in the specified period.
	 * @return - a table of data containing the transactions.
	 */
	public ResultSet loadTransactionsInPeriod(Date date1, Date date2)
	{
		ResultSet resultSet = null;
		
		try
		{
			String query = "SELECT * FROM T_Transaction WHERE T_Transaction.Date >= "
					+ " DATE('" + Convert.DateToString(date1) +"')"
					+ " AND T_Transaction.Date <= "
					+ "DATE('" + Convert.DateToString(date2) + "')";
					
			
			PreparedStatement pStatement = connection.prepareStatement(query);
			resultSet = pStatement.executeQuery();
		}
		catch(SQLException ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return resultSet;
	}
	

	/**
	 * Load the transactions from the database that occurred in the specified period.
	 * @return - a table of data containing the transactions.
	 */
	public ResultSet loadExpenseTransactionsInPeriod(Date date1, Date date2)
	{
		ResultSet resultSet = null;
		
		try
		{
			String query = "SELECT * FROM T_Transaction WHERE T_Transaction.Date >= "
					+ " DATE('" + Convert.DateToString(date1) +"')"
					+ " AND T_Transaction.Date <= "
					+ "DATE('" + Convert.DateToString(date2) + "')";
					
			
			PreparedStatement pStatement = connection.prepareStatement(query);
			resultSet = pStatement.executeQuery();
		}
		catch(SQLException ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return resultSet;
	}
	
	/**
	 * Read all the budgets from the database into a result set.
	 * @return - result set containing the categories.
	 */
	public ResultSet loadBudgets()
	{
		ResultSet resultSet = null;
		
		try
		{
			// read  categories from database
			String query = "SELECT * FROM T_Budget";

			PreparedStatement pStatement = connection.prepareStatement(query);
			
			resultSet = pStatement.executeQuery();
		}
		catch(SQLException ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return resultSet;
	}
	
	/**
	 * Read categories of the specified budget from the database into a result set.
	 * @return - result set containing the categories.
	 */
	public ResultSet loadBudgetCategories(int budgetID)
	{
		ResultSet resultSet = null;
		try
		{
			// read  categories from database
			String query = "SELECT * FROM R_Budget_Category WHERE BudgetID =" +
						Integer.toString(budgetID);

			PreparedStatement pStatement = connection.prepareStatement(query);
			resultSet = pStatement.executeQuery();
		}
		catch(SQLException ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return resultSet;
	}
	
	
	/**
	 * Insert a new budget into the database. 
	 * @param name - name of the budget.
	 * @param value - value of the budget.
	 * @param note - note of the budget.
	 * @param StartDate - start date of the budget.
	 * @param EndDate - end date of the budget.
	 * @return - the id of the new budget
	 */
	public int insertBudget(String name, BigDecimal value, String note, 
					Date StartDate, Date EndDate)
	{
		int id = -1;
		
		String query = "INSERT INTO T_Budget(Name, Amount, Note, StartDate, EndDate) " +
					"VALUES ( ?, ?, ?, DATE(?), DATE(?) )";
		
		try
		{
			// insert transaction into the database
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			
			pStatement.setString(1, name);
			pStatement.setBigDecimal(2, value);
			pStatement.setString(3,note);
			pStatement.setString(4,Convert.DateToString(StartDate));
			pStatement.setString(5,Convert.DateToString(EndDate));
			
			pStatement.executeUpdate();
					
			// read the auto generated id from the database
			ResultSet resultSet = pStatement.getGeneratedKeys();
		
			while(resultSet.next())
			{
				id = resultSet.getInt(1);
			}
			
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return id;
	}
	
	/***
	 * Insert a category for a specified budget.
	 * @param budgetId - the budget ID for which to insert the category.
	 * @param category - the category to insert
	 */
	public void insertBudgetCategory(int budgetId, String category)
	{
		String query = "INSERT INTO R_Budget_Category(BudgetID, Category) " +
				"VALUES ( ?, ? )";
		
		try
		{
			// insert transaction into the database
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			
			pStatement.setInt(1, budgetId);
			pStatement.setString(2, category);
			
			pStatement.executeUpdate();
					
			// read the auto generated id from the database
			pStatement.getGeneratedKeys();
		
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
	}
	
	/**
	 * Update transaction information in the database.
	 * @param id - the id of the budget which to update.
	 * @param name - the new name of the budget.
	 * @param value - the new value of the budget.
	 * @param note - the new note of the budget.
	 * @param StartDate - the new start date of the budget.
	 * @param EndDate - the new end date of the budget.
	 */
	public void updateBudget(int id, String name, BigDecimal value, String note, 
			Date startDate, Date endDate)
	{
		String query = "UPDATE T_Budget SET Name='" + name + "'," +
				" Amount=" + value.toString() + "," +
				" Note='" + note + "'," +
				" StartDate=" + "DATE('" + Convert.DateToString(startDate) + "')" + "," +
				" EndDate=" + "DATE('" + Convert.DateToString(endDate) + "')" +
				" WHERE ID= " + Integer.toString(id) +";";
			
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			pStatement.executeUpdate();
		
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}

	}
	
	/**
	 * Remove transaction with specified ID from the database. 
	 * Note:
	 *  The method remove the categories associated with the database.
	 * @param ID - the ID of the transaction that will be removed.
	 */
	public void removeBudget(int budgetID)
	{
		String query = "DELETE FROM T_Budget WHERE ID=" + Integer.toString(budgetID) +";";
		
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			pStatement.executeUpdate();
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
	
		query = "DELETE FROM R_Budget_Category WHERE BudgetID=" + Integer.toString(budgetID) +";";
		
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			pStatement.executeUpdate();
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
	}
	
	/**
	 * Remove the categories of the specified budget.
	 * @param budgetId - the id of the budget
	 */
	public void deleteBudgetCategories(int budgetId)
	{
		String query = "DELETE FROM R_Budget_Category WHERE BudgetID=" +
					Integer.toString(budgetId) + ";";
		
		try
		{
			PreparedStatement pStatement = this.connection.prepareStatement(query);
			pStatement.executeUpdate();
		}
		catch(SQLException ex) 
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
	}
}
