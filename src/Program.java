import javax.swing.SwingUtilities;

import gui.BudgetFrame;

/**
 * Program class 
 * 	Contains the application entry point and the main thread.
 */
public class Program implements Runnable
{
	/**
	 * The application`s entry point.
	 * @param args - unused 
	 */
	public static void main(String[] args) 
	{
		System.setProperty("sun.java2d.d3d", "false");
		System.setProperty("sun.java2d.ddoffscreen", "true");
		System.setProperty("sun.java2d.noddraw", "true");
		//System.setProperty("sun.java2d.opengl", "true");
	
		
		SwingUtilities.invokeLater(new Program());
	}

	/**
	 * The application`s main thread.
	 */
	@Override
	public void run() 
	{
		try 
		{
			// create and display the application`s main window.
			BudgetFrame budgetView = new BudgetFrame();
			
			budgetView.setVisible(true);
        } 
		catch (Exception e)  
		{
			e.printStackTrace();
		}
	}
}
