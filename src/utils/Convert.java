package utils;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EmptyStackException;

import javax.swing.JOptionPane;

import dataManager.*;

/**
 * Converts a base data type to another base data type.
  */
public class Convert 
{
	/**
	 * Converts the specified Date value to a String value.
	 * @param date - the Date to convert.
	 * @return a String that is equivalent to date.
	 */
	
	public static String DateToStringMonth(Date date)
	{
		String string = null;
		SimpleDateFormat sdformat = new SimpleDateFormat("dd MMMM yyyy");
		
		try
		{
			string = sdformat.format(date);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, e.toString());
		}
		
		return string;
	}
	
	/**
	 * Converts the specified Date value to a String value.
	 * @param date - the Date to convert.
	 * @return a String that is equivalent to date.
	 */
	
	public static String DateToString(Date date)
	{
		String string = null;
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		
		try
		{
			string = sdformat.format(date);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, e.toString());
		}
		
		return string;
	}
	
	/**
	 * Convert the specified String value to a date value.
	 * @param string - the String to convert.
	 * @return a Date that is equivalent to string.
	 */
	public static Date StringToDate(String string)
	{
		Date date = null;
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		
		try
		{
			date = sdformat.parse(string);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, e.toString());
		}
		
		return date;
	}
	
	/**
	 * Convert an integer value to a category type (income/expense)
	 */
	public static Category.Type IntToCategory(int value)
	{
		if (value == 0)
		{
			return Category.Type.Expense;
		}
		
		if (value == 1)
		{
			return Category.Type.Income;
		}
		
		throw new EmptyStackException();
	}
	
}