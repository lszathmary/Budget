package utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Provides some helpful date functions.  
 */
public class DateUtils 
{
	/**
	 * Gets yesterday`s date.
	 * @return yesterday`s date.
	 */
	public static Date getYesterday()
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(new Date());
		
		// Jump back to yesterday
		calendar.add(Calendar.DATE, -1);
		
		return calendar.getTime();
	}
	
	/**
	 * Gets last week`s date (Jump back a week, the day of the week doesn't matter).
	 * @return last week`s date.
	 */
	public static Date getLastWeek()
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(new Date());
		
		// Jump back a week
		calendar.add(Calendar.WEEK_OF_MONTH, -1);
		
		return calendar.getTime();
	}
	
	/**
	 * Gets last month`s date (Jump back a month, doesn't matter what day).
	 * @return last month`s date.
	 */
	public static Date getLastMonth()
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(new Date());
		
		// Jump back a month
		calendar.add(Calendar.MONTH, -1);
		
		return calendar.getTime();
	}
	
	/**
	 * Gets the first day of the specified week.
	 * @param date - the specified week
	 * @return first day of the week.
	 */
	public static Date getFirstDayOfWeek(Date date)
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
								
		calendar.setTime(date);
		
		// If the day is not Monday, go back on day a time until Monday
	    while(calendar.get(Calendar.DAY_OF_WEEK) != calendar.getFirstDayOfWeek())
		{	
	    	calendar.add(Calendar.DATE, -1);
		}
	    
	    return calendar.getTime();
	}
	
	/**
	 * Gets the last day of the specified week.
	 * @param date - the specified week
	 * @return first day of the week.
	 */
	public static Date getLastDayOfWeek(Date date)
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
		
		// If the day is not Monday, go back on day a time until Monday
	    while(calendar.get(Calendar.DAY_OF_WEEK) != calendar.getFirstDayOfWeek())
		{	
	    	calendar.add(Calendar.DATE, -1);
		}
	    
	    // Jump to the last day of the week
	    calendar.add(Calendar.DATE, +6);
	    
	    return calendar.getTime();
	}
	
	/**
	 * Gets the last day of the specified month.
	 * @param date - the specified month
	 * @return first day of the month
	 */
	public static Date getFirstDayOfMonth(Date date)
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
		
		// Set the first day of the month
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		
		return calendar.getTime();
	}
	
	/**
	 * Gets the last day of the specified month.
	 * @param date - the specified month
	 * @return first day of the month
	 */
	public static Date getLastDayOfMonth(Date date)
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
		
		// Set the last day of the month
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		return calendar.getTime();
	}
	
	/**
	 * Gets the first day of the specified year.
	 * @param date - the specified year
	 * @return the first day of the year
	 */
	public static Date getFirstDayOfYear(Date date)
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
		
		// Set the last day of the month
		calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
		
		return calendar.getTime();
	}
	
	/**
	 * Gets the last day of the specified year.
	 * @param date - the specified year
	 * @return the first day of the year
	 */
	public static Date getLastDayOfYear(Date date)
	{
		// Get an instance of the calendar
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(date);
		
		// Set the last day of the month
		calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
		
		return calendar.getTime();
	}
	
}
