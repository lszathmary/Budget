package dataManager;

/**
 * TransactionChangedListener
 * 	The class is used to process the event that is raised when a transaction changes, is added
 * or is removed. 
 */
public interface TransactionChangedListener
{
	/**
	 * Invoked when the TransactionChangedEvent is raised by a the DataManager.
	 * @param event theTransactionChangedEvent
	 */
	public void valueChanged(TransactionChangedEvent event);
}
