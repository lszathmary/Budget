package dataManager;



import java.awt.Color;

import javax.swing.ImageIcon;

/**
 * Category class
 * 	Represent a transaction category. A category has a name, an icon and a type. There are two 
 * types of categories: income and expense.
 * 	Examples of income categories: Salary, Gift
 *  Examples of expense categories: Food, Drinks, Gym
 *    
 * @Note
 * 	There most not be 2 categories with the same name. Each category has to have a different name.
 * 
 */
public class Category 
{
	public enum Type
	{
		Expense,
		Income,
	}
	
	private String name;
	private ImageIcon icon;
	private Type type;
	private Color color;
	

	/**
	 * Constructor.
	 */
	public Category()
	{
		this.name = "";
		this.icon = null;
	}
	
	/**
	 * Gets the name of the category.
	 * @return The name of the category.
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Sets the name of the category
	 * @param name - The name of the category
	 */
	protected void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Gets the icon of the Category 
	 * @return The icon of the category.
	 */
	public ImageIcon getIcon()
	{
		return this.icon;
	}
	
	/**
	 * Sets the icon of the category
	 * @param icon - The icon of the category
	 */
	protected void setIcon(ImageIcon icon)
	{
		this.icon = icon;
	}
	
	/**
	 * Gets the type of the category
	 * @param The type of the category
	 */
	public Type getType()
	{
		return this.type;
	}
	
	/**
	 * Sets the type of the category
	 * @param type  - The type of the category
	 */
	protected void setType(Type type)
	{
		this.type = type;
	}
	
	/**
	 * Get the color of the category.
	 * @return The color of the category
	 */
	public Color getColor()
	{
		return this.color;
	}
	
	
	/**
	 * Set the color of the category.
	 * @param RGB - The color of the category.
	 */
	public void setColor(int RGB)
	{
		this.color = new Color(RGB);
	}
	
	/**
	 * Returns a string representation of the object.
	 * @note return the name of the category, this way sorting works correctly in JTabel 
	 */
	@Override
	public String toString()
	{ 
		return this.name;
    } 
	
}
