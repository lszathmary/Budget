package dataManager;

import java.awt.Image;
import java.awt.Toolkit;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import databaseManager.DatabaseManager;
import utils.Convert;

/**
 * DatanManager class 
 * 	Class is the 'core' of the application, a 'business logic' class.
 *  
 * The Data manager class is responsible for managing:
 *   - transactions - creating, removing, updating and provides information about transaction categories
 *   - transactions events - you can add listeners that are notified when a transaction is created, removed or updated
 *   - budgets - creating, removing, updating
 *   - summaries - provides transactions summaries
 *   
 */
public class DataManager
{
	private DatabaseManager dbManager;
	
	// list and map of the transactions
	private ArrayList<Transaction>	transactionList;
	private HashMap<Integer,Transaction> transactionMap;
	
	// list of the income and expense categories
	private ArrayList<Category> incomeCategoryList;
	private ArrayList<Category> expenseCategoryList;
	
	private ArrayList<Category> categoryList;
	private HashMap<String, Category> categoryMap;
	
	// list and map of the budgets
	private ArrayList<Budget> budgetList;
	private HashMap<Integer, Budget> budgetMap;
	
	// list of transaction listeners
	private ArrayList<TransactionChangedListener> transactionListenerList;
		
	/**
	 * 	Create a new instance of the DataManager class. 
	 */
	public DataManager() 
	{
		this.dbManager = new DatabaseManager();
		
		transactionMap = new HashMap<Integer,Transaction>();
		transactionList = new ArrayList<Transaction>();
				 
		incomeCategoryList = new ArrayList<Category>();
		expenseCategoryList = new ArrayList<Category>();
				
		categoryList = new ArrayList<Category>();
		categoryMap = new HashMap<String,Category>();

		budgetList = new ArrayList<Budget>();
		budgetMap = new HashMap<Integer,Budget>();
		
		transactionListenerList = new ArrayList<TransactionChangedListener>();
	}	
		
	
	/**
	 * Setup the transaction manager and load the transactions and the transaction 
	 * categories the from the database.
	 * @Note  
	 * 	This function must be called after the class is created.
	 */
	public void initialize()
	{
		// establish connection to the database
		this.dbManager.Connect();	

		// load the data form the database
		this.loadCategories();
		this.loadTransactions();
		this.loadBudgets();
	}
		
	/**
	 * Load the list of transactions from the result set.
	 */
	private void loadTransactions()
	{
		// load the transactions form the database 
		ResultSet resultSet = this.dbManager.loadTransactions();
	
		try
		{
			Transaction transaction = null;
						
			// read a transaction from the resultSet
			while(resultSet.next())
			{
				transaction = new Transaction();
				
				transaction.setID(resultSet.getInt("ID"));
				transaction.setCategory(this.getCategorybyName(resultSet.getString("Category")));
				transaction.setValue(resultSet.getBigDecimal("Value"));
				transaction.setTag(resultSet.getString("Tag"));
				transaction.setDate(Convert.StringToDate(resultSet.getString("Date")));

				transaction.setValue(transaction.getValue().setScale(2));
				
				// put transaction in a list and a hashMap
				this.transactionList.add(transaction);
				this.transactionMap.put(transaction.getID(),transaction);
			}
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
	}
	
	/**
	 * Load the list of categories from the result set.
	 */
	private void loadCategories()
	{
		// load the categories of the expense from the database
		ResultSet resultSet = this.dbManager.loadCategories();
		
		try
		{
			Category category = null;
			
			// read a category form the result set
			while (resultSet.next())
			{
				category = new Category();
				
				byte[] date = resultSet.getBytes("Image");
				Image image = Toolkit.getDefaultToolkit().createImage(date);
				
				category.setName(resultSet.getString("Name"));
				category.setType(Convert.IntToCategory(resultSet.getInt("Type")));
				category.setIcon(new ImageIcon(image));
				category.setColor(resultSet.getInt("Color"));
		
				
				if (category.getType() ==  Category.Type.Income)
				{
					this.incomeCategoryList.add(category);
				}
				
				if (category.getType() ==  Category.Type.Expense)
				{
					this.expenseCategoryList.add(category);
				}
				
				// put the category in a list and a hashMap
				this.categoryList.add(category);
				this.categoryMap.put(category.getName(),category);
			}
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
	}
	
	/**
	 * Load the list of budgets from the result set.
	 */
	private void loadBudgets()
	{
		// load the budget from the database
		ResultSet resultSet = dbManager.loadBudgets();
		
		try
		{
			Budget budget = null;
			
			// read the budget from the result set
			while (resultSet.next())
			{
				budget = new Budget();
				
				budget.setID(resultSet.getInt("ID"));
				budget.setName(resultSet.getString("Name"));
				budget.setValue(resultSet.getBigDecimal("Amount"));
				budget.setNote(resultSet.getString("Note"));
				budget.setStartDate(Convert.StringToDate(resultSet.getString("StartDate")));
				budget.setEndDate(Convert.StringToDate(resultSet.getString("EndDate")));
				
				// load the categories of the budget
				budget.setCategories(this.loadBudgetCategories(budget.getID()));
				
				budget.setValue(budget.getValue().setScale(2));

				// put the category in a list and a hashMap
				this.budgetList.add(budget);
				this.budgetMap.put(budget.getID(), budget);
				
				this.recalculateBudget(budget);
			}
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
	}
	
	/**
	 * Load the categories of the specified budget.
	 * @param id - The id of the budget.
	 * @return The list of categories.
	 */
	private ArrayList<Category> loadBudgetCategories(int id)
	{
		ArrayList<Category> categories = new ArrayList<Category>();
		
		// load the categories of this budget form the database
		ResultSet resultSet = dbManager.loadBudgetCategories(id);
		
		try
		{
			while (resultSet.next())
			{
				// if the category field is null it means the budget applies for every 
				// expense category
				if (resultSet.getString("Category") != null)
				{
					categories.add(this.getCategorybyName(resultSet.getString("Category")));
				}
				else
				{
					categories = this.expenseCategoryList;
				}
			}
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return categories;
	}
		
	
	/**
	 * Gets the transaction at the specified index. 
	 * @param index - The zero-based index of the transaction.
	 * @return The transaction at the specified index.
	 */
	public Transaction getTransaction(int index)
	{
		return this.transactionList.get(index);
	}
	
	/**
	 * Get the transaction with the specified Id.
	 * @param id - The Id of the transaction.
	 * @return The transaction with the specified Id. 
	 */
	public Transaction getTransactionbyID(int id)
	{
		return this.transactionMap.get(id);
	}
	 
	/**
	 * Gets the number of the transactions.
	 * @return The number of the transactions. 
	 */
	public int getTransactionCount()
	{
		return this.transactionList.size();
	}

	/**
	 * Add a new transaction.
	 * @param category - The category of the transaction.
	 * @param value - The value of the transaction.
	 * @param tag - The tag of the transaction.
	 * @param date - The date of the transaction.
	 * @return The ID of the new transaction. 
	 */
	public int addTransaction(Category category, BigDecimal value, String tag, Date date)
	{
		int id = dbManager.insertTransaction(category.getName(), value, tag, date);
		
		Transaction transaction = new Transaction();
		
		transaction.setID(id);
		transaction.setCategory(category);
		transaction.setValue(value);
		transaction.setTag(tag);
		transaction.setDate(date);
		
		// put transaction in a list and a hashMap
		this.transactionList.add(transaction);
		this.transactionMap.put(transaction.getID(),transaction);
		
		// notify event listeners that a transaction has changed
		this.fireTransactionChanged(new TransactionChangedEvent());
		
		return id;
	}
	
	/**
	 * Update a transaction.
	 * @param id - The Id of the transaction which to d.
	 * @param category - The new category of the transaction.
	 * @param value - The new value of the transaction.
	 * @param tag - The new tag of the transaction.
	 * @param date - The new date of the transaction.
	 */
	public void updateTransaction(int id, Category category, BigDecimal value, String tag, Date date)
	{
		dbManager.updateTransaction(id, category.getName(), value, tag, date);
		
		Transaction transaction = this.getTransactionbyID(id);
		
		transaction.setCategory(category);
		transaction.setValue(value);
		transaction.setTag(tag);
		transaction.setDate(date);
		
		// notify event listeners that a transaction has changed
		this.fireTransactionChanged(new TransactionChangedEvent());
	}
	
	/**
	 * Remove the transaction with the specified Id.
	 * @param id - The ID of the transaction which to remove.
	 */
	public void removeTransaction(int id)
	{
		Transaction transaction = this.getTransactionbyID(id); 
		
		// remove the transaction from the database
		dbManager.removeTransaction(id);
		
		// remove the transaction from the list and map
		this.transactionList.remove(transaction);
		this.transactionMap.remove(transaction);
		
		// notify event listeners that a transaction has changed
		this.fireTransactionChanged(new TransactionChangedEvent());
	}
	
	/**
	 * Gets the first(oldest) transaction. 
	 * @return The oldest transaction.
	 */
	public Transaction getFirstTransaction()
	{
		int id = dbManager.getFirstTransactionID();
		
		return this.getTransactionbyID(id);
	}
	
	/**
	 * Gets the last(the most recent) transaction.
	 * @return The newest transaction.
	 */
	public Transaction getLastTransaction()
	{
		int id = dbManager.getLastTransactionID();
		
		return this.getTransactionbyID(id);
	}
	
	/**
	 * Returns the list of transactions that occurred in the specified period.
	 * @param startDate - The beginning of the period.
	 * @param endDate - The end of the period.
	 * @return The list of transactions that occurred in specified period. 
	 */
	public ArrayList<Transaction> getTransactionsInPeriod(Date startDate, Date endDate)
	{
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	
		if ( (startDate == null) || (endDate == null))
		{
			return transactions;
		}
		
		// read the transactions form the database 
		ResultSet resultSet = dbManager.loadTransactionsInPeriod(startDate, endDate);
			
		try
		{
			// read a transaction from the resultSet
			while(resultSet.next())
			{
				transactions.add(this.getTransactionbyID(resultSet.getInt("ID")));
			}
		}
		catch (Exception ex)
		{	
			JOptionPane.showMessageDialog(null, ex.toString());
		}
		
		return transactions;
	}
		

	/**
	 * Gets the expense category at the specified index. 	
	 * @param index - The zero-based index of the expense category.
	 * @return The expense category at the specified index.
	 */
	public Category getExpenseCategory(int index)
	{
		return this.expenseCategoryList.get(index);
	}

	/**
	 * Gets the number of expense categories..
	 * @return The number of expense categories.
	 */
	public int getExpenseCategoryCount()
	{
		return this.expenseCategoryList.size();
	}
	
	/**
	 * Returns the income category at the specified index. 	
	 * @param index - The zero-based index of the income category.
	 * @return The income category at the specified index.
	 */
	public Category getIncomeCategory(int index)
	{
		return this.incomeCategoryList.get(index);
	}
	
	/**
	 * Gets the number of income categories.
	 * @return The number of income categories.
	 */
	public int getIncomeCategoryCount()
	{
		return this.incomeCategoryList.size();
	}
	
	/**
	 * Gets the category with the specified name.
	 * @param name - the name of the category.
	 * @return The category with specified name.
	 */
	public Category getCategorybyName(String name)
	{
		return this.categoryMap.get(name);
	}
		
	/**
	 * Returns true if there is a category of any kind with the specified name.
	 * @param name - The name of the category to verify.
	 * @return The true if there is a category with the specified name
	 */
	public boolean isCategory(String name)
	{
		return this.categoryMap.containsKey(name);
	}
		
	/**
	 * Gets the list of available expense categories.
	 * @return - The expense categories.
	 */
	public ArrayList<Category> getExpenseCategories()
	{
		return this.expenseCategoryList;
	}

	/**
	 * Gets the list of available income categories. 
	 * @return - The income categories.
	 */
	public ArrayList<Category> getIncomeCategories()
	{
		return this.incomeCategoryList;
	}
	
	
	/**
	 * Gets the budget at the specified index. 
	 * @param index - The zero-based index of the budget.
	 * @return The budget at the specified index.
	 */
	public Budget getBudget(int index)
	{
		return this.budgetList.get(index);
	}
	
	/**
	 * Gets the budget with the specified ID.
	 * @param ID - The ID of the budget.
	 * @return The budget with the specified ID. 
	 */
	public Budget getBudgetbyID(Integer id)
	{
		return this.budgetMap.get(id);
	}

	/**
	 *  Return the number of budgets.
	 *  @return - The number of the budgets.
	 */
	public int getBudgetCount()
	{
		return this.budgetList.size();
	}
	
	/**
	 * Returns true if there is a Budget of any kind with the specified name.
	 * @param name - The name of the budget to verify.
	 * @return The true if there is a budget with the specified name
	 */
	public boolean isBudget(String name)
	{
		return this.budgetMap.containsKey(name);
	}
		
	/**
	 * Add a new budget.
	 * @param name - The name of the budget.
	 * @param value - The value of the budget.
	 * @param note - The note of the budget.
	 * @param startDate - The start date of the budget.
	 * @param endDate - The end date of the budget.
	 * @param categories - The categories of the budget.
	 * @return The id of the budget.
	 */
	public int addBudget(String name, BigDecimal value, String note, 
					Date startDate, Date endDate, ArrayList<Category> categories)
	{
		// add budget to the database
		int id = dbManager.insertBudget(name, value, note, startDate, endDate);
	
		if (id == -1 ) 
		{
			return id;
		}
		
		if (categories.size() == this.expenseCategoryList.size())
		{
			dbManager.insertBudgetCategory(id, null);
		}
		else
		{
			// add budget categories to the database
			for (int i = 0; i< categories.size(); i++)
			{
				dbManager.insertBudgetCategory(id, categories.get(i).getName());
			}
		}
		
		Budget budget = new Budget();
		
		budget.setID(id);
		budget.setName(name);
		budget.setValue(value);
		budget.setNote(note);
		budget.setStartDate(startDate);
		budget.setEndDate(endDate);
		budget.setCategories(categories);
		
		// put transaction in a list and a hashMap
		this.budgetList.add(budget);
		this.budgetMap.put(id, budget);
		
		return id;
	}
	
	/**
	 * Update a budget
	 * @param ID - the ID of the the budget which to updated.
	 * @param anem - The name of the budget.
	 * @param value - The new value of the budget.
	 * @param note - The note of the budget.
	 * @param startDate - The start date of the budget.
	 * @param endDate - The end date of the budget.
	 * @param categories - The categories of the budget.
	 */
	public void updateBudget(int id, String name, BigDecimal value, String note, 
			Date startDate, Date endDate, ArrayList<Category> categories)
	{
		// update the budget in the database
		dbManager.updateBudget(id, name, value, note, startDate, endDate);
		
		Budget budget = this.getBudgetbyID(id);
				
		budget.setName(name);
		budget.setValue(value);
		budget.setNote(note);
		budget.setStartDate(startDate);
		budget.setEndDate(endDate);
		budget.setCategories(categories);

		// remove all of the old budget categories from the database than add the new ones.
		dbManager.deleteBudgetCategories(id);
		
		
		if (categories.size() == this.expenseCategoryList.size())
		{
			dbManager.insertBudgetCategory(id, null);
		}
		else
		{
			for (int i = 0; i< categories.size(); i++)
			{
				dbManager.insertBudgetCategory(id, categories.get(i).getName());
			}
		}
		
		recalculateBudget(budget);
	}
	
	/**
	 * Remove the budget with the specified id.
	 * @param id - The ID of the budget which to remove.
	 */
	public void removeBudget(Integer id)
	{
		Budget budget = this.getBudgetbyID(id);
		
		// remove the budget from the database
		dbManager.removeBudget(budget.getID());
		
		// remove the budget from the list and map
		budgetList.remove(budget);
		budgetMap.remove(budget);
	}
	
	/**
	 * Calculates the current value of the budget - meaning how much has been spent in the 
	 * period specified by the budget.
	 * @param budget - The specified budget.
	 */
	public void recalculateBudget(Budget budget)
	{
		BigDecimal currentValue = new BigDecimal(0);
		
		// List of that match the criteria of the budget
		ArrayList<Transaction> transactions =  this.getTransactionOfBudget(budget);
		
		// sum up the transactions
		for (int i=0; i<transactions.size(); i++)
		{	
			currentValue = currentValue.add(transactions.get(i).getValue());
		}
			
		
		// store the sum in the current value
		budget.setCurrentValue(currentValue);
	}
	
	/**
	 * Recalculate all of the budgets..
	 */
	public void recalculateBudgets()
	{
		for (Budget budget:this.budgetList)
		{
			this.recalculateBudget(budget);
		}
	}
		
	/**
	 * Gets the transaction of the specified budget. In other words the transaction that
	 * occurred in the time period and the categories specified by the budget.
	 * @param budget - The specified budget.
	 * @return The list of transactions.
	 */
	public ArrayList<Transaction> getTransactionOfBudget(Budget budget)
	{
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();

		// list of transaction that occurred in the specified period
		transactions = getTransactionsInPeriod(budget.getStartDate(), budget.getEndDate());
		
		for(Iterator<Transaction> i = transactions.iterator(); i.hasNext();) 
		{
			Transaction transaction = i.next();

			// remove the transactions with categories that are not specified in the category list
			// of the budget
			if (!budget.getCategories().contains(transaction.getCategory()))
			{
				i.remove();
			}
		}
			
		return transactions;
	}
	
	
	/**
	 * Add a Transaction listener.
	 * @param listener - The listener to be added.
	 */
	public void addTransactionListener(TransactionChangedListener listener)
	{
		this.transactionListenerList.add(listener);
	}

	/**
	 * Notifies all listeners that have registered interest for notification on this event type.
	 * @param event - The event that occurred.
	 */
	private void fireTransactionChanged(TransactionChangedEvent event)
	{
		for (int i=0; i<transactionListenerList.size(); i++ )
		{
			transactionListenerList.get(i).valueChanged(event);
		}
	}
	
	
	/**
	 * Sums up the value of the have expense transaction by category in the specified period.
	 *  
	 * @param startDate - The beginning of the period.
	 * @param endDate - The end of the period.
	 * @return - The sum of transactions for each expense category.
	 * 
	 * @note
	 * For example lets say you had the following transactions in the given period: 
	 *    Food: 40.00, Food: 5.00, Drinks: 4.00, Drinks: 2.00,
	 * This function would return: The category named Food with the value of 45.00 and the category
	 * named Drinks with the value of 7.00.
	 */
	public LinkedHashMap<Category,BigDecimal> getExpenseTransactionsSummary(Date startDate, Date endDate)
	{
		// using LinkedHashMap because the categories have to be in a fixed order
		// every time the summary is calculated
		LinkedHashMap<Category,BigDecimal> expCategorySumMap = new LinkedHashMap<Category,BigDecimal>();
		
		// all the transaction in the specified period
		ArrayList<Transaction> transactions = getTransactionsInPeriod(startDate, endDate);
		
		
		
		// for each expense category that exits 
		for(Category category: getExpenseCategories())
		{
			
			
			BigDecimal sum = new BigDecimal(0);
						
			// go through the transactions and sum up the values of the transactions with the current category
			for (Transaction transaction : transactions) 
			{
				if (transaction.getCategory().getName().equals(category.getName()))
				{
					sum = sum.add(transaction.getValue());
				}
			}
			
			
			
			// if there are no transactions in this category don`t add it to the map
			if (!sum.equals(new BigDecimal(0)))
			{
				expCategorySumMap.put(category, sum);
			}
		}
		
		return expCategorySumMap;
	}
		
	/**
	 * Gets the summary of the of income transactions in the specified period.
	 * @param startDate - The beginning of the period.
	 * @param endDate - The end of the period.
	 * @return - The sum of transactions for each income category.
	 */
	public LinkedHashMap<Category,BigDecimal> getIncomeTransactionSummary(Date startDate, Date endDate)
	{
		
		// the categories have to be in a fixed order, so they appear in the same order in the chart
		LinkedHashMap<Category,BigDecimal> inCategorySumMap = new LinkedHashMap<Category,BigDecimal>();
		
				
		// the transaction in the specified period
		ArrayList<Transaction> transactions = getTransactionsInPeriod(startDate, endDate);
				
				
		// for each income category
		for(Category category: getIncomeCategories())
		{
			BigDecimal sum = new BigDecimal(0);
			
								
			// go through the transactions and sum up the values of the transactions with the given category
			for (Transaction transaction : transactions) 
			{
				if (transaction.getCategory().getName().equals(category.getName()))
				{
					sum = sum.add(transaction.getValue());
				}
			}
				
					
			// if there are no transactions in this category don`t add it to the map
			if (!sum.equals(new BigDecimal(0)))
			{
				inCategorySumMap.put(category, sum);
			}
		}
				
		return inCategorySumMap;
	}
	
	/**
	 * Get the sum of all the income transactions in the specified period. 
	 * @param startDate
	 * @param endDate
	 * @return The sum of income transactions.
	 */
	public BigDecimal getSumOfIncomeTransactions(Date startDate, Date endDate)
	{
		BigDecimal sum = new BigDecimal(0);
		// the transaction in the specified period
		ArrayList<Transaction> transactions = getTransactionsInPeriod(startDate, endDate);
		
		// go through the transactions and sum up the values of the transactions with the given category
		for (Transaction transaction : transactions) 
		{
			if (transaction.getType() == Category.Type.Income)
			{
				sum = sum.add(transaction.getValue());
			}
		}
		
		return sum;
	}
	
	/**
	 * Get the sum of all the income transactions in the specified period. 
	 * @param startDate
	 * @param endDate
	 * @return The sum of income transactions.
	 */
	public BigDecimal getSumOfExpenseTransactions(Date startDate, Date endDate)
	{
		BigDecimal sum = new BigDecimal(0);
		// the transaction in the specified period
		ArrayList<Transaction> transactions = getTransactionsInPeriod(startDate, endDate);
		
		// go through the transactions and sum up the values of the transactions with the given category
		for (Transaction transaction : transactions) 
		{
			if (transaction.getType() == Category.Type.Expense)
			{
				sum = sum.add(transaction.getValue());
			}
		}
		
		return sum;
	}
}
