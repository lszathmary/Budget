package dataManager;


import java.math.BigDecimal;
import java.util.Date;


/**
 * Transaction class 
 * 	Represents a transaction made by the user. 
 * A transaction has an id, a category, a value, a date on which the transaction occurred and a type. 
 * For now there are 2 type of transactions: expense, like Food, Drinks, Gym, Car 
 * and income like Salary, Gift, ExtraIncome
 * 
 * Example of an expense transaction:
 *	id = 1
 *  category = food
 *  value = 10.00
 *  date = 1999-09-09
 *
 * This means the user brought Food for $ 10.00 on the date of 1999-09-09. 
 * 
 * @Note
 * 	The id of the transaction is auto generated.
 */
public class Transaction 
{
	private int id;
	private Category category;
	private BigDecimal value;
	private String tag;
	private Date date;
		
	/**
	 * Creates a transaction.
	 */
	public Transaction()
	{
		// do nothing
	}
	
	/**
	 * Returns the identifier of this transaction.
	 */
	public int getID()
	{
		return this.id;
	}
	
	/** 
	 * Sets the identifier of this transaction.
	 */
	protected void setID(int ID)
	{
		this.id = ID;
	}
		
	/**
	 * Returns the category of this transaction.
	 */
	public Category getCategory()
	{
		return this.category;
	}
	
	/**
	 * Sets the category of this transaction.
	 */
	protected void setCategory(Category category)
	{
		this.category = category;
	}
	
	/**
	 * Sets the value of the transaction. (the amount of money)
	 */
	public BigDecimal getValue()
	{
		return this.value;
	}
	
	/**
	 * Returns the value of the transaction.(the amount of money)
	 */
	protected void setValue(BigDecimal value)
	{
		this.value = value;
	}
	
	/**
	 * Returns the tag of the transaction.
	 */
	public String getTag()
	{
		return this.tag;
	}

	/**
	 * Sets the tag of transaction.
	 */
	protected void setTag(String tag)
	{
		this.tag = tag; 
	}
	
	/**
	 * Returns the date on which the transaction occurred.
	 */
	public Date getDate()
	{
		return this.date;
	}
	
	/**
	 * Sets the date on which the transaction occurred.
	 */
	protected void setDate(Date date)
	{
		this.date = date;
	}
	
	/**
	 * Returns the type of the transaction.
	 */
	public Category.Type getType()
	{
		return this.category.getType();
	}
}
