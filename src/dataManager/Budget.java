package dataManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/** 
 * 
 * Budget class 
 * 	Represents a budget made by the user. 
 * A budget has: an id, a name, a value (limit), a note, a start date and an end Date, and a list of categories
 * to which it applies. 
 *  
 * Example of a budget of 100.00 for Food in the month of January.
 *  Budget 
 *    id: auto generated
 *    name: "test"
 *    value: 100.00
 *    start date: 2016-01-01
 *    end date: 2016-01-31
 *    categories: Food
 * 
 * @Note
 * 	The id of the transaction is auto generated.
 *  You can`t modify the budget directly, that is why the set functions are protected, you have do it through
 *  the DataManager class.
 */
public class Budget 
{
	private int id;
	private String name;
	private BigDecimal value;
	private BigDecimal currentValue;
	private String note;
	private Date startDate;
	private Date endDate;
	private ArrayList<Category> categories;
			
	/**
	 * Constructor.
	 */
	public Budget()
	{
		categories =  new ArrayList<Category>();
	}

	/**
	 * Get the identifier of the budget.
	 * @return The identifier of this budget.
	 */
	public int getID()
	{
		return this.id;
	}
	
	/**
	 * Sets the identifier of the budget.
	 * @param id - the new id of the budget.
	 */
	protected void setID(int id)
	{
		this.id = id;
	}
	
	/**
	 * Gets the name of the budget.
	 * @return The name of this budget.
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name of the budget.
	 * @param name - The new name of the budget.
	 */
	protected void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Gets the note of the budget.
	 * @return The note of this budget.
	 */
	public String getNote()
	{
		return this.note;
	}

	/**
	 * Sets the note of the budget.
	 * @param note - The new note of the budget.
	 */
	protected void setNote(String note)
	{
		this.note = note;
	}
	
	/**
	 * Gets the value of the budget.
	 * @return The value of this budget.
	 */
	public BigDecimal getValue()
	{
		return this.value;
	}

	/**
	 * Sets the value of the budget.
	 * @param value - The new value of the budget.
	 */
	protected void setValue(BigDecimal value)
	{
		this.value = value;
	}
	
	/**
	 * Gets the current value of the budget.
	 * @return The current value of this budget.
	 */
	public BigDecimal getCurrentValue()
	{
		return this.currentValue;
	}
	
	/**
	 * Sets the current value of the budget.
	 * @param value - The new current value of the budget.
	 */
	public void setCurrentValue(BigDecimal value)
	{
		this.currentValue = value;
	}
	
	/**
	 * Gets the categories of this budget.
	 * @return The categories of the budget.
	 */
	public ArrayList<Category> getCategories()
	{
		return this.categories;
	}
	
	/**
	 * Set the categories of the budget.
	 * @param categories - The new categories of the budget.
	 */
	public void setCategories(ArrayList<Category> categories)
	{
		this.categories = categories;
	}

	/**
	 * Gets the start date of the budget.
	 * @return The start date of this budget.
	 */
	public Date getStartDate()
	{
		return this.startDate;
	}
	
	/**
	 * Sets the start date of the budget.
	 * @param date The new start date of the budget.
	 */
	protected void setStartDate(Date date)
	{
		this.startDate = date;
	}	

	/**
	 * Gets the end date of the budget.
	 * @return The end date of this budget.
	 */
	public Date getEndDate()
	{
		return this.endDate;
	}
	
	/**
	 * Sets the end date of the budget.
	 * @param date The new end date of the budget.
	 */
	protected void setEndDate(Date date)
	{
		this.endDate = date;
	}

}
